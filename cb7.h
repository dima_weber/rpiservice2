#ifndef CB7_H_INCLUDED
#define CB7_H_INCLUDED

#include <stdint.h>
#include "abstractsensor.h"

class CB7Sensor : public GPIOAbstractSensor
{
    std::string valName;
public:
    CB7Sensor(const char* sensorName, const std::string& valueName, GPIO::RPiGPIOPin pin);

protected:
    virtual bool cleanup() override;
    virtual bool init() override;
    virtual bool execute() override;
};

#endif // CB7_H_INCLUDED
