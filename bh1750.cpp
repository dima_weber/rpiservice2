#include "bh1750.h"
#include "tools/tools.h"
#include "drivers/i2c.h"
#include <iostream>
#include <iomanip>
#include <memory>

#define BH1750_ADDRESS                 0x23
#define BH1750_CMD_POWERDOWN           0x00
#define BH1750_CMD_POWERON             0x01
#define BH1750_CMD_RESET               0x03


#define BH1750_CMD_MESURE_TIME_HIGH         0x40
#define BH1750_CMD_MESURE_TIME_HIGH_MASK    0xE0
#define BH1750_CMD_MESURE_TIME_LOW     0x60
#define BH1750_CMD_MESURE_TIME_LOW_MASK    0x1F

#define BH1750_ACCURACY  10 / 12
#define BH1750_DEFAUL_MTREG_VALUE 0x45

#define BH1750_HR_MODE_MEASURE_DELAY_MS 120
#define BH1750_LR_MODE_MEASURE_DELAY_MS 24

BH1750Sensor::BH1750Sensor(const char* sensorName)
    : I2CAbstractSensor(sensorName, BH1750_ADDRESS, BH1750_HR_MODE_MEASURE_DELAY_MS)
{
    registerValue(SensorRecord::Luminosity);
}

void BH1750Sensor::setMode(BH1750Sensor::MeasureMode mesMode, BH1750Sensor::PeriodicMode period)
{
    mode = 0 | mesMode | period;
}

void BH1750Sensor::setSensetivity(uint8_t sens)
{
    if (sens < 31)
        this->sens = 31;
    else if  (sens > 255)
        this->sens = 255;
    else
        this->sens = sens;

    uint8_t high = ((this->sens & BH1750_CMD_MESURE_TIME_HIGH_MASK) >> 5) | BH1750_CMD_MESURE_TIME_HIGH;
    uint8_t low =   (this->sens & BH1750_CMD_MESURE_TIME_LOW_MASK)        | BH1750_CMD_MESURE_TIME_LOW;

    minimalDelay.tv_sec = 0;
    minimalDelay.tv_usec = measureDelay() * 1000;

    i2c->write8(high);
    i2c->write8(low);
}

uint8_t BH1750Sensor::sensetivity() const
{
    return sens;
}

bool BH1750Sensor::cleanup()
{
    i2c->write8(BH1750_CMD_POWERDOWN);
    return I2CAbstractSensor::cleanup();
}

bool BH1750Sensor::init()
{
    //
    i2c->write8(BH1750_CMD_POWERON);
    setMode(HiRes, Continous);
    setSensetivity(BH1750_DEFAUL_MTREG_VALUE);
    return I2CAbstractSensor::init();
}

bool BH1750Sensor::execute()
{
    if ((mode & OneTime) == OneTime)
        i2c->write8(BH1750_CMD_POWERON);

//    std::cout << "use mode: " << std::hex << std::setw(2) <<std::setfill('0') << (int)mode << std::dec << std::endl;
    i2c->write8 (mode);
    rpi_delay(measureDelay() );
    uint32_t lum =0;
//    uint8_t byte0 = i2c->read8();
//    uint8_t byte1 = i2c->read8();
//    lum = (int)byte0 * 256 + (int)byte1;
    lum = revert16(i2c->reg_get16(mode));
//    std::cout << name() << "sens: " << (int)sens << " measureDelay: " << measureDelay() << " raw value: " << lum << std::endl;
    uint32_t luminosity = lum * 100  * BH1750_DEFAUL_MTREG_VALUE * BH1750_ACCURACY / sensetivity();
    switch (mode & MeasureMask)
    {
        case LowRes     : luminosity *= 4; break;
        case UltraHiRes : luminosity /= 2; break;
        case HiRes      : break;
    }

    setValue(SensorRecord::Luminosity, luminosity);

    return true;
}

uint16_t BH1750Sensor::measureDelay() const
{
    if ((mode & LowRes) == LowRes)
        return  BH1750_LR_MODE_MEASURE_DELAY_MS * sensetivity() / BH1750_DEFAUL_MTREG_VALUE;
    else
        return  BH1750_HR_MODE_MEASURE_DELAY_MS * sensetivity() / BH1750_DEFAUL_MTREG_VALUE;
}
