#ifndef ADS1115_H
#define ADS1115_H

#include "abstractsensor.h"

class ADS1115 : public I2CAbstractSensor
{
public:
    ADS1115(const char* sensorName);
    ~ADS1115();

protected:
    virtual bool execute() override;
    virtual bool init() override;
    virtual bool cleanup() override;

};

#endif // ADS1115_H
