#include "bmp.h"
#include "drivers/i2c.h"
#include "tools/tools.h"
#include <iostream>
#include <iomanip>
#include <exception>
#include <memory>

#define BMP180_READ_ADDRESS 0x77
#define BMP180_WRITE_ADDRESS 0x77

#define BMP085_REG_CALIBRATION_DATA_ADDR 0xAA
#define BMP085_REG_CHIPID       0xD0
#define BMP085_REG_VERSION      0xD1
#define BMP085_CMD_CONTROL      0xf4
#define BMP085_REG_TEMPDATA     0xf6
#define BMP085_REG_PRESDATA     0xf6
#define BMP085_CMD_READTEMP     0x2e
#define BMP085_CMD_READPRES     0x34
#define BMP085_CMD_SOFTRESET    0xE0

BMP180::BMP180(const char* sensorName, BMP180::Oversampling oversampling)
    : I2CAbstractSensor(sensorName, BMP180_READ_ADDRESS, 0), oversampling(oversampling)
{
    // timedelay for temp: 4.5 ms
    // for pressure:
    //  oversampling  delay
    //  0               4.5
    //  1               7.5
    //  2              13.5
    //  3              25.5
    // but no xtra delay should be performed because we block for this time anyway
    registerValue(SensorRecord::Temperature);
    registerValue(SensorRecord::Pressure);
}

bool BMP180::cleanup()
{
    i2c->reg_set8(BMP085_CMD_SOFTRESET, 0xB6);
    return I2CAbstractSensor::cleanup();
}

bool BMP180::init()
{
    I2CAbstractSensor::init();
    // get id/version
    chipId  =  i2c->reg_get8(BMP085_REG_CHIPID);
    version =  i2c->reg_get8(BMP085_REG_VERSION);

    // get calibration data
    ac1 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR));
    ac2 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 2));
    ac3 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 4));
    ac4 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 6));
    ac5 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 8));
    ac6 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 10));
     b1 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 12));
     b2 = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 14));
     mb = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 16));
     mc = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 18));
     md = revert16(i2c->reg_get16(BMP085_REG_CALIBRATION_DATA_ADDR + 20));

    return chipId == 0x55;
}

bool BMP180::execute()
{
    int32_t b3, b5, b6, x1, x2, x3, p;
    uint32_t b4, b7;
    uint16_t tempRawData = 27898;
    int32_t presRawData = 23843;

    // get raw temperature
    i2c->reg_set8(BMP085_CMD_CONTROL, BMP085_CMD_READTEMP);
    rpi_delay(5);
    tempRawData = revert16(i2c->reg_get16(BMP085_REG_TEMPDATA));

//        char tempReadBuf[2];
//        i2c->reg_get_block(BMP085_REG_TEMPDATA, tempReadBuf, sizeof(tempReadBuf));
//        tempRawData = (int)tempReadBuf[0] << 8 | (int)tempReadBuf[1];

    // get raw pressure
    i2c->reg_set8(BMP085_CMD_CONTROL, BMP085_CMD_READPRES + (oversampling << 6));
    rpi_delay(2 + (3 << oversampling));

    presRawData =  revert16(i2c->reg_get16(BMP085_REG_PRESDATA));
    presRawData <<= 8;
    presRawData |= i2c->reg_get8(BMP085_REG_PRESDATA+2);

//        char presReadBuf[3];
//        i2c->reg_get_block(BMP085_REG_PRESDATA, presReadBuf, sizeof(presReadBuf));
//        presRawData = (int)presReadBuf[0] << 16 | (int)presReadBuf[1] << 8 | (int)presReadBuf[2];

    presRawData >>= (8-oversampling);

    // calculate real temp / press
    x1 = ((int)tempRawData - (int)ac6) * ((int)ac5) >> 15;
    if (x1 == -md)
        throw std::runtime_error("invalid data");
    x2 = ((int)mc << 11) / (x1+md);
    b5 = x1 + x2;

    b6 = b5 - 4000;

    x1 = (b2 *  (b6 * b6)>>12 ) >> 11;
    x2 = (ac2 * b6) >> 11;
    x3 = x1 + x2;
    b3 = ((((int)ac1*4 + x3) << oversampling) + 2) / 4;

    x1 = (ac3 * b6) >> 13;
    x2 = (b1 * ((b6 * b6) >> 12)) >> 16;
    x3 = ((x1 + x2) + 2) >> 2;
    b4 = (ac4 * (uint32_t)(x3 + 32768)) >> 15;
    if (b4 == 0)
        throw std::runtime_error ("invalid data");

    b7 = ((uint32_t)(presRawData - b3) * ( 50000UL >> oversampling ));

    if (b7 < 0x80000000)
    {
        p = (b7 * 2) / b4;
    }
    else
    {
        p = (b7 / b4) * 2;
    }
    x1 = (p >> 8) * (p >> 8);
    x1 = (x1 * 3038) >> 16;
    x2 = (-7357 * p) >> 16;

    int32_t pres = p + ((x1 + x2 + 3791)>>4);
    int16_t temp = (b5 + 8) >> 4 ;

    setValue(SensorRecord::Temperature, temp);
    setValue(SensorRecord::Pressure, pres);

#ifdef DEBUG_BMP
    std::cout << "chipid: " << (int)chipId
               << " version: " << (int)version
               << std::hex
               << " Calibration data: "
               << " ac1=" << std::setfill('0') << std::setw(4) << ac1 << " ac2=" << std::setw(4) << ac2
               << " ac3=" << std::setw(4) << ac3 << " ac4=" << std::setw(4) << ac4
               << " ac5=" << std::setw(4) << ac5 << " ac6=" << std::setw(4) << ac6
               << "  b1=" << std::setw(4) <<  b1 << "  b2=" << std::setw(4) << b2
               << "  mb=" << std::setw(4) <<  mb << "  mc=" << std::setw(4) << mc
               << "  md=" << std::setw(4) << md
               << " Raw data: "
               << " rt=" << std::setw(4) << tempRawData
               << " rp=" << std::setw(6) << presRawData
               << std::dec
               << std::endl;
#endif // DEBUG_BMP
    return true;
}
