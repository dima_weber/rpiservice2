#ifndef DHT_H_INCLUDED
#define DHT_H_INCLUDED

#include <stdint.h>
#include "abstractsensor.h"

//#define DEBUG_DHT
struct sched_param;


class DHT22Sensor : public GPIOAbstractSensor
{
public:
    DHT22Sensor(const char* sensorName, GPIO::RPiGPIOPin pin);

protected:
    virtual bool cleanup() override;
    virtual bool init() override;
    virtual bool execute() override;
};

#endif // DHT_H_INCLUDED
