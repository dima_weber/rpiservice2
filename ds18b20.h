#ifndef DS18B20_H
#define DS18B20_H

#include "abstractsensor.h"

#include <stdint.h>
#include <bitset>
#include <vector>
#include <memory>
#include <map>

class DS18B20Sensor;
class OneWire
{
public:
    typedef std::bitset<64> DeviceID;
    typedef std::shared_ptr<DS18B20Sensor> DS18B20SensorPtr;

    struct DeviceIDComparer {
        bool operator() (const DeviceID &b1, const DeviceID &b2) const {
            return b1.to_ullong() < b2.to_ullong();
        }
    };

    std::vector<DeviceID> discovered_1wire_Id;
    std::map<DeviceID, DS18B20SensorPtr, OneWire::DeviceIDComparer> discovered_devices;

    OneWire (GPIO::RPiGPIOPin pin);

    void write_1_time_slot();
    void write_0_time_slot();
    bool read_time_slot();
    bool reset();
    uint8_t read8 ();
    void write8(uint8_t cmd);
    void search_devices(uint8_t searchCmd, std::vector<DeviceID> &discoveredId, const DeviceID& partial_id =0, size_t partial_size =0);
    void send_rom (const DeviceID& id);
    std::vector<DS18B20SensorPtr> devices()
    {
        std::vector<DS18B20SensorPtr> list;
        for(auto& elem: discovered_devices)
            list.push_back(elem.second);
        return list;
    }
    void setAllLowAlarm(int8_t lowTemp);
    void setAllHighAlarm(int8_t highTemp);
    void setAllAlarmRange(int8_t lowTemp, int8_t highTemp);

    std::vector<DS18B20SensorPtr> alarmed();

    bool isParasite();
private:
    GPIOPtr gpio;
};

class DS18B20Sensor : public AbstractSensor
{
    /// TODO: Add alarm support
    OneWire& onewire; /// TODO: unique pointer here is a bug. should be shared or raw
    OneWire::DeviceID deviceId;
public:
    int8_t scratchpad_th;
    int8_t scratchpad_tl;

    void setLowAlarm(int8_t temp);
    void setHighAlarm(int8_t temp);
    void setAlarmRange(int8_t low, int8_t high);
protected:
    DS18B20Sensor(const char* sensorName, OneWire& oneWire, OneWire::DeviceID id);

    virtual bool init() override;
    virtual bool cleanup() override;
    virtual bool execute() override;

    friend class OneWire;
};

#endif // DS18B20_H
