#include "abstractsensor.h"

#include <iostream>
#include <system_error>

#include "tools/tools.h"
#include "tools/time_util.h"

std::ostream& operator<<(std::ostream& stream, const SensorRecord& rec)
{
    stream << rec.toString();
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const PollResult& res)
{
    if (res.empty())
        stream << "NA";
    for (auto& rec: res)
        stream << rec.second << " ";
    return stream;
}

AbstractSensor::AbstractSensor(const char* sensorName, uint16_t minimalDelayMs)
    : failReason(), valuesMap(), initialized(false), sensorName(sensorName), lastExecuteTime(), minimalDelay()
{
    minimalDelay.tv_sec= minimalDelayMs / 1000;
    minimalDelay.tv_usec= (minimalDelayMs % 1000) * 1000;
}

bool AbstractSensor::poll()
{
    bool ret = false;
    failReason.clear();
    invalidateValues();

    try
    {
        timeval pollTime;
        timeval timeSinceLastPoll;
        struct timezone tz;
        gettimeofday(&pollTime, &tz);
        timeSinceLastPoll = pollTime - lastExecuteTime;
        if (timeSinceLastPoll < minimalDelay)
            throw std::runtime_error("not ready");
        lastExecuteTime = pollTime;
        ret = (initialized || reset()) && execute();
    }
    catch (std::runtime_error& err)
    {
        failReason = err.what();
    }
    return ret;
}

bool AbstractSensor::reset(bool force)
{
    if (initialized || force)
        cleanup();
    return init();
}

std::string AbstractSensor::display() const
{
    std::stringstream stream;
    stream << name();
    if (!valuesMap.empty())
    {
        stream << ": " << values();
    }
    if (!failReason.empty())
        stream << " [problem]: " << failReason;
    stream << std::endl;
    return stream.str();
}

std::string AbstractSensor::onlyValuesDisplay() const
{
    std::stringstream stream;
    if (!valuesMap.empty())
    {
        for (auto& rec : values())
            stream << std::setw(WIDTH) << rec.second.valueToString() << DELIMITER;
    }
    else
        for (auto& type: parameters())
            stream << std::setw(15) << "NA" << DELIMITER;
    return stream.str();
}

std::vector<std::string> AbstractSensor::parameters() const
{
    std::vector<std::string> params;
    for  (auto& rec: valuesMap)
    {
        params.push_back(rec.first);
    }
    return params;
}

bool AbstractSensor::init()
{
    initialized = true;
    return true;
}

bool AbstractSensor::cleanup()
{
    initialized = false;
    return true;
}

void AbstractSensor::registerValue(const std::string &resName, SensorRecord::SensType type)
{
    SensorRecord rec;
    rec.type = type;
    valuesMap[resName] = rec;
}

void AbstractSensor::registerValue(SensorRecord::SensType type)
{
    registerValue( SensorRecord::name(type), type);
}

void AbstractSensor::invalidateValues()
{
    /*
    if (cachedResult)
      result.clear();
    */
    for (auto& rec: valuesMap)
        rec.second.valid = false;
}

AbstractSensor::~AbstractSensor()
{
    cleanup();
}

void AbstractSensor::setValue(const std::string& resName, uint32_t value)
{
    SensorRecord& rec = valuesMap[resName];
    rec.value = value;
    rec.valid = true;
}

void AbstractSensor::setValue(SensorRecord::SensType type, uint32_t value)
{
    setValue(SensorRecord::name(type), value);
}

void AbstractSensor::setValue(const std::string& resName, const std::tm& datetime)
{
    SensorRecord& rec = valuesMap[resName];
    rec.datetime = datetime;
    rec.valid = true;
}

void AbstractSensor::setValue(SensorRecord::SensType type, const tm &value)
{
    setValue(SensorRecord::name(type), value);
}

/////////////////

I2CAbstractSensor::I2CAbstractSensor(const char* sensorName, uint8_t deviceAddr, uint16_t minimalDelayMs)
    : AbstractSensor(sensorName, minimalDelayMs), i2c(nullptr)
{
    i2c.reset(getI2CDriver(deviceAddr));
}

I2CAbstractSensor::~I2CAbstractSensor()
{
    i2c.reset();
}

bool I2CAbstractSensor::poll()
{
    bool ret = false;
    try
    {
        ret = AbstractSensor::poll();
    }
    catch(std::system_error& err)
    {
        std::stringstream os;
        os << name() << ": " << err.what() << " : ";
        switch (err.code().value())
        {
//            case BCM2835_I2C_REASON_ERROR_NACK: os << "Received a NACK"; break;
//            case BCM2835_I2C_REASON_ERROR_CLKT: os << "Received Clock Stretch Timeout"; break;
//            case BCM2835_I2C_REASON_ERROR_DATA: os << "Not all data is sent / received"; break;
            default:                            os << "Unknown error"; break;
        }
        failReason = os.str();
    }
    return ret;
}


GPIOAbstractSensor::GPIOAbstractSensor(const char *sensorName, GPIO::RPiGPIOPin pin, uint16_t minmalDelayMs)
    : AbstractSensor(sensorName, minmalDelayMs)
{
    gpio.reset(getGPIODriver(pin));
}
