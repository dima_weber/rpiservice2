#ifndef BMP_H_INCLUDED
#define BMP_H_INCLUDED

#include <stdint.h>
#include "abstractsensor.h"

//#define DEBUG_BMP
//#define BMP_DIAGNOSTIC


class BMP180 : public I2CAbstractSensor
{
public:
    enum Oversampling {BMP085_ULTRALOWPOWER=0,BMP085_STANDARD=1,BMP085_HIGHRES=2, BMP085_ULTRAHIGHRES=3};

    BMP180(const char* sensorName, Oversampling oversampling = BMP085_STANDARD);
    void setOversampling(Oversampling oversampling = BMP085_STANDARD);

protected:
    uint8_t oversampling;

    virtual bool  cleanup() override;
    virtual bool     init() override;
    virtual bool execute () override;

private:
    uint8_t     chipId  = 0;
    uint8_t     version = 0;
    int16_t     ac1 = 0,
                ac2 = 0,
                ac3 = 0;
    uint16_t    ac4 = 0,
                ac5 = 0,
                ac6 = 0;
    int16_t     b1  = 0,
                b2  = 0,
                mb  = 0,
                mc  = 0,
                md  = 0;

};
#endif // BMP_H_INCLUDED
