#include "ads1115.h"
#include "tools/tools.h"
#include <sstream>

#define ADS1115_REG_CONVERSATION 0x00
#define ADS1115_REG_CONFIG 0x01
#define ADS1115_REG_LO_THRESH 0x02
#define ADS1115_REG_HI_THRESH 0x03

/** OS: Operational status/single-shot conversion start

    This bit determines the operational status of the device.
    This bit can only be written when in power-down mode.
    For a write status:
        0 : No effect
        1 : Begin a single conversion (when in power-down mode)
    For a read status:
        0 : Device is currently performing a conversion
        1 : Device is not currently performing a conversion
*/
#define ADS1115_CFG_OS_MASK   0x8000
#define ADS1115_CMD_BEGIN_CONV  (0xFFFF & ADS1115_CFG_OS_MASK)
#define ADS1115_STATUS_BUSY 0x0000
#define ADS1115_STATUS_READY 0x80000

/** MUX[2:0]: Input multiplexer configuration (ADS1115 only)

    These bits configure the input multiplexer. They serve no function on the ADS1113/4.
        000 : AINP = AIN0 and AINN = AIN1 (default) 100 : AINP = AIN0 and AINN = GND
        001 : AINP = AIN0 and AINN = AIN3           101 : AINP = AIN1 and AINN = GND
        010 : AINP = AIN1 and AINN = AIN3           110 : AINP = AIN2 and AINN = GND
        011 : AINP = AIN2 and AINN = AIN3           111 : AINP = AIN3 and AINN = GND
*/
#define ADS1115_CFG_MUX2_MASK 0x4000
#define ADS1115_CFG_MUX1_MASK 0x2000
#define ADS1115_CFG_MUX0_MASK 0x1000
#define ADS1115_CFG_MUX_MASK  (ADS1115_CFG_MUX0_MASK | ADS1115_CFG_MUX1_MASK | ADS1115_CFG_MUX2_MASK)
#define ADS1115_CMD_MUX_01   0x0000
#define ADS1115_CMD_MUX_03   0x1000
#define ADS1115_CMD_MUX_13   0x2000
#define ADS1115_CMD_MUX_23   0x3000
#define ADS1115_CMD_MUX_0G   0x4000
#define ADS1115_CMD_MUX_1G   0x5000
#define ADS1115_CMD_MUX_2G   0x6000
#define ADS1115_CMD_MUX_3G   0x7000
#define ADS1115_CMD_MUX_DEFAULT ADS1115_CMD_MUX_01

/** PGA[2:0]: Programmable gain amplifier configuration (ADS1114 and ADS1115 only)

    These bits configure the programmable gain amplifier. They serve no function on the ADS1113.
        @value 000 : FS = ±6.144V @warning This parameter expresses the full-scale range of the ADC scaling. In no event should more than VDD + 0.3V be applied to this device
        @value 001 : FS = ±4.096V @warning This parameter expresses the full-scale range of the ADC scaling. In no event should more than VDD + 0.3V be applied to this device
        @value 010 : FS = ±2.048V (default)
        @value 011 : FS = ±1.024V
        @value 100 : FS = ±0.512V
        @value 101 : FS = ±0.256V
        @value 110 : FS = ±0.256V
        @value 111 : FS = ±0.256V
 */
#define ADS1115_CFG_PGA2_MASK 0x0800
#define ADS1115_CFG_PGA1_MASK 0x0400
#define ADS1115_CFG_PGA0_MASK 0x0200
#define ADS1115_CFG_PGA_MASK (ADS1115_CFG_PGA0_MASK | ADS1115_CFG_PGA1_MASK | ADS1115_CFG_PGA2_MASK)
#define ADS1115_CMD_PGA_6V 0x0000
#define ADS1115_CMD_PGA_4V 0x0200
#define ADS1115_CMD_PGA_2V 0x0400
#define ADS1115_CMD_PGA_1V 0x0600
#define ADS1115_CMD_PGA_05V 0x0800
#define ADS1115_CMD_PGA_02V 0x0A00
#define ADS1115_CMD_PGA_021V 0x0C00
#define ADS1115_CMD_PGA_022V 0x0E00
#define ADS1115_CMD_PGA_DEFAULT ADS1115_CMD_PGA_2V

/** MODE: Device operating mode

    This bit controls the current operational mode of the ADS1113/4/5.
        @value 0 : Continuous conversion mode
        @value 1 : Power-down single-shot mode (default)
*/
#define ADS1115_CFG_MODE_MASK 0x0100
#define ADS1115_CMD_MODE_CONT 0x0000
#define ADS1115_CMD_MODE_SNGL 0x0100
#define ADS1115_CMD_MODE_DEFAULT ADS1115_CMD_MODE_SNGL

/** DR[2:0]: Data rate

    These bits control the data rate setting.
        @value 000 : 8SPS     @value  100 : 128SPS (default)
        @value 001 : 16SPS    @value  101 : 250SPS
        @value 010 : 32SPS    @value  110 : 475SPS
        @value 011 : 64SPS    @value  111 : 860SPS
*/
#define ADS1115_CFG_DR2_MASK  0x0080
#define ADS1115_CFG_DR1_MASK  0x0040
#define ADS1115_CFG_DR0_MASK  0x0020
#define ADS1115_CFG_DR_MASK  (ADS1115_CFG_DR0_MASK  | ADS1115_CFG_DR1_MASK | ADS1115_CFG_DR2_MASK)
#define ADS1115_CMD_DR_8 0x0000
#define ADS1115_CMD_DR_16 0x0020
#define ADS1115_CMD_DR_32 0x0040
#define ADS1115_CMD_DR_64 0x0060
#define ADS1115_CMD_DR_128 0x0080
#define ADS1115_CMD_DR_250 0x00A0
#define ADS1115_CMD_DR_475 0x00C0
#define ADS1115_CMD_DR_860 0x00E0
#define ADS1115_CMD_DR_DEFAULT ADS1115_CMD_DR_128

/** COMP_MODE: Comparator mode (ADS1114 and ADS1115 only)

    This bit controls the comparator mode of operation. It changes whether the comparator is implemented as a
    traditional comparator (COMP_MODE = '0') or as a window comparator (COMP_MODE = '1'). It serves no
    function on the ADS1113.
        @value 0 : Traditional comparator with hysteresis (default)
        @value 1 : Window comparator
*/
#define ADS1115_CFG_COMP_MODE_MASK  0x0010
#define ADS1115_CMD_COMP_MODE_TRADITIONAL 0x0000
#define ADS1115_CMD_COMP_MODE_WINDOW 0x0010
#define ADS1115_CMD_COMP_MODE_DEFAULT ADS1115_CMD_COMP_MODE_TRADITIONAL

/** COMP_POL: Comparator polarity (ADS1114 and ADS1115 only)
    This bit controls the polarity of the ALERT/RDY pin. When COMP_POL = '0' the comparator output is active
    low. When COMP_POL='1' the ALERT/RDY pin is active high. It serves no function on the ADS1113.
        @value 0 : Active low (default)
        @value 1 : Active high
*/
#define ADS1115_CFG_COMP_POL_MASK  0x0008
#define ADS1115_CFG_COMP_POL_LOW 0x0000
#define ADS1115_CFG_COMP_POL_HIGH 0x0008
#define ADS1115_CFG_COMP_POL_DEFAULT ADS1115_CFG_COMP_POL_LOW

/** COMP_LAT: Latching comparator (ADS1114 and ADS1115 only)

    This bit controls whether the ALERT/RDY pin latches once asserted or clears once conversions are within the
    margin of the upper and lower threshold values. When COMP_LAT = '0', the ALERT/RDY pin does not latch
    when asserted. When COMP_LAT = '1', the asserted ALERT/RDY pin remains latched until conversion data
    are read by the master or an appropriate SMBus alert response is sent by the master, the device responds with
    its address, and it is the lowest address currently asserting the ALERT/RDY bus line. This bit serves no
    function on the ADS1113.
        0 : Non-latching comparator (default)
        1 : Latching comparator
*/
#define ADS1115_CFG_COMP_LAT_MASK  0x0004
#define ADS1115_CMD_COMP_LAT_OFF  0x0000
#define ADS1115_CMD_COMP_LAT_ON  0x0004
#define ADS1115_CMD_COMP_LAT_DEFAULT  ADS1115_CMD_COMP_LAT_OFF

/** COMP_QUE: Comparator queue and disable (ADS1114 and ADS1115 only)

    These bits perform two functions. When set to '11', they disable the comparator function and put the
    ALERT/RDY pin into a high state. When set to any other value, they control the number of successive
    conversions exceeding the upper or lower thresholds required before asserting the ALERT/RDY pin. They
    serve no function on the ADS1113.
        00 : Assert after one conversion
        01 : Assert after two conversions
        10 : Assert after four conversions
        11 : Disable comparator (default)
*/
#define ADS1115_CFG_COMP_QUE1_MASK  0x0002
#define ADS1115_CFG_COMP_QUE0_MASK  0x0001
#define ADS1115_CFG_COMP_QUE_MASK (ADS1115_CFG_COMP_QUE0_MASK | ADS1115_CFG_COMP_QUE1_MASK)
#define ADS1115_CMD_COMP_QUEUE_1 0x0000
#define ADS1115_CMD_COMP_QUEUE_2 0x0001
#define ADS1115_CMD_COMP_QUEUE_4 0x0002
#define ADS1115_CMD_COMP_QUEUE_OFF 0x0003
#define ADS1115_CMD_COMP_QUEUE_DEFAULT ADS1115_CMD_COMP_QUEUE_OFF

ADS1115::ADS1115(const char* sensorName)
   :I2CAbstractSensor(sensorName, 0x48, 1)
{
    for (int i=0; i<4; i++)
    {
        std::stringstream stream;
        stream << "general_" << i;
        registerValue(stream.str(), SensorRecord::General);
    }
}

ADS1115::~ADS1115()
{

}

bool ADS1115::execute()
{
    uint16_t res;
    const std::vector<uint16_t> analogRegisters = {ADS1115_CMD_MUX_0G, ADS1115_CMD_MUX_1G, ADS1115_CMD_MUX_2G, ADS1115_CMD_MUX_3G};

    for (int i=0; i< analogRegisters.size(); i++)
    {
        i2c->reg_set16(ADS1115_REG_CONFIG,    ADS1115_CMD_BEGIN_CONV   | analogRegisters[i]      | ADS1115_CMD_PGA_2V
                                            | ADS1115_CMD_MODE_SNGL    | ADS1115_CMD_DR_DEFAULT  | ADS1115_CMD_COMP_MODE_DEFAULT
                                            | ADS1115_CFG_COMP_POL_DEFAULT | ADS1115_CMD_COMP_LAT_DEFAULT | ADS1115_CMD_COMP_QUEUE_OFF);
        do
        {
            res = i2c->reg_get16(ADS1115_REG_CONFIG);
        } while ((res & ADS1115_CFG_OS_MASK) == ADS1115_STATUS_BUSY);

        res = revert16(i2c->reg_get16(ADS1115_REG_CONVERSATION));

        int32_t value = (res * 100) / 0x7FFF;

        std::stringstream stream;
        stream << "general_" << i;
        setValue(stream.str(), value);
    }
    return true;
}

bool ADS1115::init()
{
    I2CAbstractSensor::init();
    return true;
}

bool ADS1115::cleanup()
{
    return I2CAbstractSensor::cleanup();
}



