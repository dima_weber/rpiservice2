// *** ADDED BY HEADER FIXUP ***
#include <ctime>
// *** END ***
#include "hc-sr04.h"
#include "tools/tools.h"
#include "tools/time_util.h"

#include <iostream>
#include <iomanip>


HC_SR04Sensor::HC_SR04Sensor(const char* sensorName, GPIO::RPiGPIOPin trigPin, GPIO::RPiGPIOPin echoPin)
    : GPIOAbstractSensor(sensorName, echoPin, 0), temperature(0)
{
    requestGPIO.reset(getGPIODriver(trigPin));
    registerValue(SensorRecord::Distance);
}

bool HC_SR04Sensor::cleanup()
{
    return GPIOAbstractSensor::cleanup();
}

uint16_t HC_SR04Sensor::getSoundVelosity() const
{
    // cm/s  0* temp
    // every 1* increase speed by 59 cm/s
    return 33150 + temperature * 59 / 10;
}

bool  HC_SR04Sensor::init()
{
    requestGPIO->setOutput();
    gpio->setInput();
    gpio->pudUp();
    return GPIOAbstractSensor::init();
}

bool HC_SR04Sensor::execute()
{
    // maximal distance for hc-sr04 is 400cm, which gives measure time ~ 11-13 ms
    TimeVal pulse_timeout (0, 130000);

    TimeVal pulse_start;
    TimeVal pulse_end;
    struct timeval pulse_delay;

    long soundSpeed = getSoundVelosity();

    requestGPIO->low(1000);
    requestGPIO->high(10);
    requestGPIO->low();

    bool timeout = false;

    try
    {
        pulse_start = gpio->waitPin(GPIO::High, pulse_timeout);
        pulse_end = gpio->waitPin(GPIO::Low, pulse_timeout);

        pulse_delay = pulse_end - pulse_start;
        long distance = (pulse_delay.tv_sec * soundSpeed  + pulse_delay.tv_usec * soundSpeed / 1000000) / 2 ;
        setValue(SensorRecord::Distance, distance);
    }
    catch (std::overflow_error& err)
    {
        timeout = true;
       // std::cout << "timeout" << std::endl;
    }
    return !timeout;
}
