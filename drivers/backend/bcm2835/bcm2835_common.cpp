#include <bcm2835.h>
#include <iostream>
#include <stdexcept>

int bcm2835_clientCount  = 0;
int bcm2835_i2c_clientCount = 0;

void bcm2835_addClient()
{
    if (bcm2835_clientCount++ == 0)
    {
        //bcm2835_set_debug(1);
        std::cout   << "Using libbcm2835 version " << bcm2835_version() << std::endl;
        if (!bcm2835_init())
        {
            std::cout   << "fail to init bcm2835 library" << std::endl;
            bcm2835_clientCount--;
            throw std::runtime_error("fail to initialize bcm2835 lib");
        }
    }
}

void bcm2835_removeClient()
{
    if (--bcm2835_clientCount == 0)
        bcm2835_close();
}


void bcm2835_addI2CClient()
{
    bcm2835_addClient();
    if (bcm2835_i2c_clientCount++ == 0)
        bcm2835_i2c_begin();
}


void bcm2835_removeI2CClient()
{
    if (--bcm2835_i2c_clientCount == 0)
        bcm2835_i2c_end();
    bcm2835_removeClient();
}
