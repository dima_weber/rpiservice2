#include "drivers/backend/bcm2835/gpio/bcm2835_gpio.h"
#include "drivers/backend/bcm2835/bcm2835_common.h"
#include <bcm2835.h>

GPIO_bcm2835lib::GPIO_bcm2835lib(GPIO::RPiGPIOPin pin)
    :GPIO(pin)
{
    bcm2835_addClient();
}

GPIO_bcm2835lib::~GPIO_bcm2835lib()
{
    bcm2835_removeClient();
}

void GPIO_bcm2835lib::set(GPIO::PinState val, const TimeVal& keep)
{
    bcm2835_gpio_write(pinNum, val);
    GPIO::set(val, keep);
}

GPIO::PinState GPIO_bcm2835lib::state()
{
    return (bcm2835_gpio_lev(pinNum) == HIGH)? GPIO::High : GPIO::Low;
}

void GPIO_bcm2835lib::setMode(GPIO::PinMode mode)
{
    bcm2835_gpio_fsel(pinNum, mode);
}

void GPIO_bcm2835lib::setPud(GPIO::PinPud pud)
{
    bcm2835_gpio_set_pud(pinNum, pud);
}
