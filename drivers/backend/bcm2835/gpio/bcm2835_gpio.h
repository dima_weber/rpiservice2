#include "drivers/gpio.h"

class GPIO_bcm2835lib: public GPIO
{
public:
    GPIO_bcm2835lib(RPiGPIOPin pin);
    ~GPIO_bcm2835lib();

    virtual void set(PinState val, const TimeVal& keep = TimeVal::Zero) override;
    virtual PinState state()  override;
    virtual void setMode (PinMode mode) override;
    virtual void setPud(PinPud pud) override;
};
