#include "drivers/i2c.h"

class I2C_bcm2835lib : public I2C
{
public:
    I2C_bcm2835lib(uint8_t deviceAddr);
    virtual ~I2C_bcm2835lib();
    virtual void     write8 (uint8_t byte) override;
    virtual uint8_t  read8 () override;

    virtual void write16(uint16_t word) override;
    virtual uint16_t read16 ();

    virtual size_t   reg_get_block(uint8_t reg, char* buf, int len) override;

protected:
    void setClockDivider (uint16_t divider );
    void setBaudrate(uint32_t baudrate);
};
