#include "drivers/backend/bcm2835/i2c/bcm2835_i2c.h"
#include "drivers/backend/bcm2835/bcm2835_common.h"
#include <bcm2835.h>
#include <system_error> // for std::system_error

I2C_bcm2835lib::I2C_bcm2835lib(uint8_t deviceAddr)
    : I2C(deviceAddr)
{
    bcm2835_addI2CClient();
}

I2C_bcm2835lib::~I2C_bcm2835lib()
{
    bcm2835_removeI2CClient();
}

void I2C_bcm2835lib::write8(uint8_t byte)
{
    uint8_t i2c_response;
    bcm2835_i2c_setSlaveAddress(deviceAddr);

    i2c_response = bcm2835_i2c_write(reinterpret_cast<char*>(&byte), sizeof(byte));
    if (i2c_response != BCM2835_I2C_REASON_OK)
        throw std::system_error(i2c_response, std::generic_category(), "fail to write byte");
    #ifdef I2C_RAW_DATA
    std::cout << "device <--   " << std::setw(2) << std::setfill('0') << std::hex << (int)byte << std::dec << std::endl;
    #endif // I2C_RAW_DATA
}

uint8_t I2C_bcm2835lib::read8()
{
    uint8_t byte;
    uint8_t i2c_response;
    bcm2835_i2c_setSlaveAddress(deviceAddr);
    i2c_response = bcm2835_i2c_read(reinterpret_cast<char*>(&byte), sizeof(byte));
    if (i2c_response != BCM2835_I2C_REASON_OK)
        throw std::system_error(i2c_response, std::generic_category(), "fail to read byte");
    #ifdef I2C_RAW_DATA
    std::cout << "device   --> " << std::setw(2) << std::setfill('0') << std::hex << (int)byte << std::dec << std::endl;
    #endif // I2C_RAW_DATA
    return byte;
}

size_t I2C_bcm2835lib::reg_get_block(uint8_t reg, char* buf, int len)
{
    if (len > 32)
        len = 32;
    uint8_t i2c_response;
    bcm2835_i2c_setSlaveAddress(deviceAddr);
    i2c_response = bcm2835_i2c_read_register_rs(reinterpret_cast<char*>(&reg), buf, len);
    if (i2c_response != BCM2835_I2C_REASON_OK)
        throw std::system_error(i2c_response, std::generic_category(), "fail to read reg");
    #ifdef I2C_RAW_DATA
    std::cout << "device [" << std::setw(2) << std::setfill('0') << std::hex << (int)reg << "] -> ";
    for(int i=0; i<len; i++)
        std::cout << " " << std::setw(2) << std::setfill('0') << (int)buf[i];
    std::cout << std::dec << std::endl;
    #endif // I2C_RAW_DATA
    return len;
}

void I2C_bcm2835lib::setClockDivider(uint16_t divider)
{
    bcm2835_i2c_setClockDivider(divider);
}

void I2C_bcm2835lib::setBaudrate(uint32_t baudrate)
{
    bcm2835_i2c_set_baudrate(baudrate);
}

uint16_t I2C_bcm2835lib::read16()
{
    uint16_t word;
    uint8_t i2c_response;
    bcm2835_i2c_setSlaveAddress(deviceAddr);
    i2c_response = bcm2835_i2c_read(reinterpret_cast<char*>(&word), sizeof(word));
    if (i2c_response != BCM2835_I2C_REASON_OK)
        throw std::system_error(i2c_response, std::generic_category(), "fail to read byte");
    #ifdef I2C_RAW_DATA
    std::cout << "device   --> " << std::setw(2) << std::setfill('0') << std::hex << (int)word << std::dec << std::endl;
    #endif // I2C_RAW_DATA
    return word;
}

void I2C_bcm2835lib::write16(uint16_t word)
{
    uint8_t i2c_response;
    bcm2835_i2c_setSlaveAddress(deviceAddr);
    i2c_response = bcm2835_i2c_write(reinterpret_cast<char*>(&word), sizeof(word));
    if (i2c_response != BCM2835_I2C_REASON_OK)
        throw std::system_error(i2c_response, std::generic_category(), "fail to read byte");
    #ifdef I2C_RAW_DATA
    std::cout << "device   --> " << std::setw(2) << std::setfill('0') << std::hex << (int)word << std::dec << std::endl;
    #endif // I2C_RAW_DATA
}
