extern int bcm2835_clientCount;
extern int bcm2835_i2c_clientCount;

void bcm2835_addClient();
void bcm2835_removeClient();

void bcm2835_addI2CClient();
void bcm2835_removeI2CClient();
