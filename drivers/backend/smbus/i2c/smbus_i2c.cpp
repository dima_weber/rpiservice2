#include <smbus.h>
#include "drivers/backend/smbus/i2c/smbus_i2c.h"
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <unistd.h>
#include <system_error>

I2C_smbus::I2C_smbus(const std::string& filename, uint8_t deviceAddr)
    :I2C(deviceAddr), i2cFile(-1)
{
    i2cFile = open(filename.c_str(), O_RDWR);
    if (i2cFile < 0)
    {
        throw std::system_error(errno, std::generic_category(), "fail to open i2c device");
    }
    if (ioctl(i2cFile, I2C_SLAVE, deviceAddr) < 0)
    {
        throw std::system_error(errno, std::generic_category(), "fail to ioctl i2c device");
    }

}

void I2C_smbus::write8 (uint8_t byte)
{
    int32_t ret = i2c_smbus_write_byte(i2cFile, byte);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to write byte to device");
    #ifdef I2C_RAW_DATA
    std::cout << "device <--   " << std::setw(2) << std::setfill('0') << std::hex << (int)byte << std::dec << std::endl;
#endif // I2C_RAW_DATA
}

uint16_t I2C_smbus::read16()
{
    uint16_t ans;
    ans = (read8() << 8) +  read8();
    return ans;
}

void I2C_smbus::write16(uint16_t word)
{
    write8(word >> 8);
    write8 (word & 0xFF);
}

uint8_t I2C_smbus::read8 ()
{
    int32_t ret = i2c_smbus_read_byte(i2cFile);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to read byte from device");
    #ifdef I2C_RAW_DATA
    std::cout << "device   --> " << std::setw(2) << std::setfill('0') << std::hex << (int)ret << std::dec << std::endl;
    #endif // I2C_RAW_DATA
    return ret & 0x000000FF;
}

size_t I2C_smbus::reg_get_block(uint8_t reg, char* buf, int len)
{
    int32_t ret = i2c_smbus_read_i2c_block_data(i2cFile, reg, len, reinterpret_cast<uint8_t*>(buf));
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to read block from device");
    #ifdef I2C_RAW_DATA
    std::cout << "device [" << std::setw(2) << std::setfill('0') << std::hex << (int)reg << "] -> ";
    for(int i=0; i<len; i++)
        std::cout << " " << std::setw(2) << std::setfill('0') << (int)buf[i];
    std::cout << std::dec << std::endl;
    #endif // I2C_RAW_DATA
    return (uint32_t)ret;
}

I2C_smbus::~I2C_smbus()
{
    close(i2cFile);
}

uint8_t I2C_smbus::reg_get8(uint8_t reg)
{
    int32_t ret = i2c_smbus_read_byte_data(i2cFile, reg);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to read byte from device register");
    return ret & 0x000000FF;
}

void I2C_smbus::reg_set8(uint8_t reg, uint8_t data)
{
    int32_t ret = i2c_smbus_write_byte_data(i2cFile, reg, data);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to write byte to device register");
}

uint16_t I2C_smbus::reg_get16(uint8_t reg)
{
    int32_t ret = i2c_smbus_read_word_data(i2cFile, reg);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to read word from device register");
    return ret & 0x0000FFFF;
}

void I2C_smbus::reg_set16(uint8_t reg, uint16_t data)
{
    int32_t ret = i2c_smbus_write_word_data(i2cFile, reg, data);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to write word to device register");
}

size_t I2C_smbus::reg_get_array(uint8_t reg, uint8_t* buffer)
{
    int32_t ret = i2c_smbus_read_block_data(i2cFile, reg, buffer);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to read array from device register");
    return (uint32_t)ret;
}

size_t I2C_smbus::reg_set_array(uint8_t reg, const uint8_t* buffer, size_t len)
{
    int32_t ret = i2c_smbus_write_block_data(i2cFile, reg, len, buffer);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to write array to device register");
    return (uint32_t)ret;
}

size_t I2C_smbus::reg_set_block(uint8_t reg, const uint8_t* buffer, size_t len)
{
    int32_t ret = i2c_smbus_write_i2c_block_data(i2cFile, reg, len, buffer);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), "fail to write block to device register");
    return (uint32_t)ret;
}
