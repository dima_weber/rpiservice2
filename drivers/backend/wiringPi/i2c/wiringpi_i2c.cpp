#include "drivers/backend/wiringPi/i2c/wiringpi_i2c.h"
#include <wiringPiI2C.h>
#include <unistd.h>
#include <system_error>

I2C_wiringPi::I2C_wiringPi(uint8_t deviceAddr)
    :I2C(deviceAddr)
{
    i2cFile = wiringPiI2CSetup(deviceAddr);
    if (i2cFile < 0)
        throw std::system_error(errno, std::generic_category(), __PRETTY_FUNCTION__);
}

I2C_wiringPi::~I2C_wiringPi()
{
    close(i2cFile);
}

uint8_t I2C_wiringPi::read8()
{
    int ret;
    ret = wiringPiI2CRead(i2cFile);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), __PRETTY_FUNCTION__);
    return ret & 0xFF;
}

void I2C_wiringPi::write16(uint16_t word)
{
    throw std::runtime_error("I2C_wiringPi::write16 not implemented");
}

uint16_t I2C_wiringPi::read16()
{
    throw std::runtime_error("I2C_wiringPi::read16 not implemented");
}

void I2C_wiringPi::write8(uint8_t byte)
{
    int ret;
    ret = wiringPiI2CWrite(i2cFile, byte);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), __PRETTY_FUNCTION__);
}

uint8_t  I2C_wiringPi::reg_get8(uint8_t reg)
{
    int ret;
    ret = wiringPiI2CReadReg8(i2cFile, reg);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), __PRETTY_FUNCTION__);
    return ret & 0xFF;
}

void     I2C_wiringPi::reg_set8(uint8_t reg, uint8_t data)
{
    int ret;
    ret = wiringPiI2CWriteReg8(i2cFile, reg, data);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), __PRETTY_FUNCTION__);
}

uint16_t  I2C_wiringPi::reg_get16(uint8_t reg)
{
    int ret;
    ret = wiringPiI2CReadReg16(i2cFile, reg);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), __PRETTY_FUNCTION__);
    return ret;
}

void     I2C_wiringPi::reg_set16(uint8_t reg, uint16_t data)
{
    int ret;
    ret = wiringPiI2CWriteReg16(i2cFile, reg, data);
    if (ret < 0)
        throw std::system_error(errno, std::generic_category(), __PRETTY_FUNCTION__);
}

size_t I2C_wiringPi::reg_get_block(uint8_t reg, char *buf, int len)
{
    int ret = 0;
    write8(reg);
    int limit = (len < 32)?len:32;
    int counter = 0;
    for(counter=0;counter<limit;counter++)
        buf[counter] = read8();
    return counter;
}

