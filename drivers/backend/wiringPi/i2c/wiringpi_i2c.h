#include "drivers/i2c.h"

class I2C_wiringPi : public I2C
{
    int i2cFile;
public:
    I2C_wiringPi (uint8_t deviceAddr);
    virtual ~I2C_wiringPi();

    virtual void write8(uint8_t byte) override;
    virtual uint8_t read8() override;

    virtual void write16(uint16_t word) override;
    virtual uint16_t read16() override;

    virtual uint8_t  reg_get8(uint8_t reg) override;
    virtual void     reg_set8(uint8_t reg, uint8_t data) override;

    virtual uint16_t reg_get16(uint8_t reg) override;
    virtual void     reg_set16(uint8_t reg, uint16_t data);

    virtual size_t   reg_get_block(uint8_t reg, char* buf, int len) override;
};
