#include "drivers/gpio.h"

class GPIO_wiringPi : public GPIO
{
    static int driverCount;
public:
    GPIO_wiringPi(GPIO::RPiGPIOPin pin);
    ~GPIO_wiringPi();

    virtual void set(PinState val, const TimeVal& keep = TimeVal::Zero) override;
    virtual PinState state()  override;
    virtual void setMode (PinMode mode) override;
    virtual void setPud(PinPud pud) override;
};
