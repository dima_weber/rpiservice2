#include "drivers/backend/wiringpi_gpio.h"
#include <wiringPi.h>

int GPIO_wiringPi::driverCount = 0;

GPIO_wiringPi::GPIO_wiringPi(GPIO::RPiGPIOPin pin)
    :GPIO(pin)
{
    if (driverCount++ == 0)
    {
        if (wiringPiSetupPhys() != 0)
        {
            std::cout   << "fail to init wiringpi library" << std::endl;
            driverCount--;
            throw std::runtime_error("fail to initialize wiringpi lib");
        }
    }
}

GPIO_wiringPi::~GPIO_wiringPi()
{

}

void GPIO_wiringPi::set(GPIO::PinState val, const TimeVal& keep)
{
    digitalWrite(pinNum, (val == High)? HIGH : LOW);
    GPIO::set(val, keep);
}

GPIO::PinState GPIO_wiringPi::state()
{
    int ret = digitalRead(pinNum);
    return (ret & 0xFF == 1)?High:Low;
}

void GPIO_wiringPi::setMode(GPIO::PinMode mode)
{
    switch (mode)
    {
        case Input: pinMode(pinNum, INPUT); break;
        case Output: pinMode(pinNum, OUTPUT); break;
        case Alt0:  if (pinNum == 7)
                        pinMode(pinNum, GPIO_CLOCK);
                    else
                        throw std::runtime_error("unsupported mode / wrong pin");
                    break;
        case Alt5:  if (pinNum == 12)
                        pinMode(pinNum, PWM_OUTPUT);
                    else
                        throw std::runtime_error("unsupported mode / wrong pin");
                    break;
        default:
                throw std::runtime_error("unsupported mode / wrong pin");
    }
}

void GPIO_wiringPi::setPud(GPIO::PinPud pud)
{
    switch (pud)
    {
        case Off: pullUpDnControl(pinNum, PUD_OFF); break;
        case Up: pullUpDnControl(pinNum, PUD_UP); break;
        case Down: pullUpDnControl(pinNum, PUD_DOWN); break;
    }
}
