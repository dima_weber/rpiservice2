#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>
#include <memory>
#include "tools/time_util.h"

class GPIO
{
public:
    /*! \brief GPIO Pin Numbers

      Here we define Raspberry Pin GPIO pins on P1 in terms of the underlying BCM GPIO pin numbers.
      These can be passed as a pin number to any function requiring a pin.
      Not all pins on the RPi 26 bin IDE plug are connected to GPIO pins
      and some can adopt an alternate function.
      RPi version 2 has some slightly different pinouts, and these are values RPI_V2_*.
      RPi B+ has yet differnet pinouts and these are defined in RPI_BPLUS_*.
      At bootup, pins 8 and 10 are set to UART0_TXD, UART0_RXD (ie the alt0 function) respectively
      When SPI0 is in use (ie after bcm2835_spi_begin()), SPI0 pins are dedicated to SPI
      and cant be controlled independently.
      If you are using the RPi Compute Module, just use the GPIO number: there is no need to use one of these
      symbolic names
    */
    typedef enum
    {
        RPI_GPIO_P1_03        =  0,  /*!< Version 1, Pin P1-03 */
        RPI_GPIO_P1_05        =  1,  /*!< Version 1, Pin P1-05 */
        RPI_GPIO_P1_07        =  4,  /*!< Version 1, Pin P1-07 */
        RPI_GPIO_P1_08        = 14,  /*!< Version 1, Pin P1-08, defaults to alt function 0 UART0_TXD */
        RPI_GPIO_P1_10        = 15,  /*!< Version 1, Pin P1-10, defaults to alt function 0 UART0_RXD */
        RPI_GPIO_P1_11        = 17,  /*!< Version 1, Pin P1-11 */
        RPI_GPIO_P1_12        = 18,  /*!< Version 1, Pin P1-12, can be PWM channel 0 in ALT FUN 5 */
        RPI_GPIO_P1_13        = 21,  /*!< Version 1, Pin P1-13 */
        RPI_GPIO_P1_15        = 22,  /*!< Version 1, Pin P1-15 */
        RPI_GPIO_P1_16        = 23,  /*!< Version 1, Pin P1-16 */
        RPI_GPIO_P1_18        = 24,  /*!< Version 1, Pin P1-18 */
        RPI_GPIO_P1_19        = 10,  /*!< Version 1, Pin P1-19, MOSI when SPI0 in use */
        RPI_GPIO_P1_21        =  9,  /*!< Version 1, Pin P1-21, MISO when SPI0 in use */
        RPI_GPIO_P1_22        = 25,  /*!< Version 1, Pin P1-22 */
        RPI_GPIO_P1_23        = 11,  /*!< Version 1, Pin P1-23, CLK when SPI0 in use */
        RPI_GPIO_P1_24        =  8,  /*!< Version 1, Pin P1-24, CE0 when SPI0 in use */
        RPI_GPIO_P1_26        =  7,  /*!< Version 1, Pin P1-26, CE1 when SPI0 in use */

        /* RPi Version 2 */
        RPI_V2_GPIO_P1_03     =  2,  /*!< Version 2, Pin P1-03 */
        RPI_V2_GPIO_P1_05     =  3,  /*!< Version 2, Pin P1-05 */
        RPI_V2_GPIO_P1_07     =  4,  /*!< Version 2, Pin P1-07 */
        RPI_V2_GPIO_P1_08     = 14,  /*!< Version 2, Pin P1-08, defaults to alt function 0 UART0_TXD */
        RPI_V2_GPIO_P1_10     = 15,  /*!< Version 2, Pin P1-10, defaults to alt function 0 UART0_RXD */
        RPI_V2_GPIO_P1_11     = 17,  /*!< Version 2, Pin P1-11 */
        RPI_V2_GPIO_P1_12     = 18,  /*!< Version 2, Pin P1-12, can be PWM channel 0 in ALT FUN 5 */
        RPI_V2_GPIO_P1_13     = 27,  /*!< Version 2, Pin P1-13 */
        RPI_V2_GPIO_P1_15     = 22,  /*!< Version 2, Pin P1-15 */
        RPI_V2_GPIO_P1_16     = 23,  /*!< Version 2, Pin P1-16 */
        RPI_V2_GPIO_P1_18     = 24,  /*!< Version 2, Pin P1-18 */
        RPI_V2_GPIO_P1_19     = 10,  /*!< Version 2, Pin P1-19, MOSI when SPI0 in use */
        RPI_V2_GPIO_P1_21     =  9,  /*!< Version 2, Pin P1-21, MISO when SPI0 in use */
        RPI_V2_GPIO_P1_22     = 25,  /*!< Version 2, Pin P1-22 */
        RPI_V2_GPIO_P1_23     = 11,  /*!< Version 2, Pin P1-23, CLK when SPI0 in use */
        RPI_V2_GPIO_P1_24     =  8,  /*!< Version 2, Pin P1-24, CE0 when SPI0 in use */
        RPI_V2_GPIO_P1_26     =  7,  /*!< Version 2, Pin P1-26, CE1 when SPI0 in use */

        /* RPi Version 2, new plug P5 */
        RPI_V2_GPIO_P5_03     = 28,  /*!< Version 2, Pin P5-03 */
        RPI_V2_GPIO_P5_04     = 29,  /*!< Version 2, Pin P5-04 */
        RPI_V2_GPIO_P5_05     = 30,  /*!< Version 2, Pin P5-05 */
        RPI_V2_GPIO_P5_06     = 31,  /*!< Version 2, Pin P5-06 */

        /* RPi B+ J8 header */
        RPI_BPLUS_GPIO_J8_03     =  2,  /*!< B+, Pin J8-03 */
        RPI_BPLUS_GPIO_J8_05     =  3,  /*!< B+, Pin J8-05 */
        RPI_BPLUS_GPIO_J8_07     =  4,  /*!< B+, Pin J8-07 */
        RPI_BPLUS_GPIO_J8_08     = 14,  /*!< B+, Pin J8-08, defaults to alt function 0 UART0_TXD */
        RPI_BPLUS_GPIO_J8_10     = 15,  /*!< B+, Pin J8-10, defaults to alt function 0 UART0_RXD */
        RPI_BPLUS_GPIO_J8_11     = 17,  /*!< B+, Pin J8-11 */
        RPI_BPLUS_GPIO_J8_12     = 18,  /*!< B+, Pin J8-12, can be PWM channel 0 in ALT FUN 5 */
        RPI_BPLUS_GPIO_J8_13     = 27,  /*!< B+, Pin J8-13 */
        RPI_BPLUS_GPIO_J8_15     = 22,  /*!< B+, Pin J8-15 */
        RPI_BPLUS_GPIO_J8_16     = 23,  /*!< B+, Pin J8-16 */
        RPI_BPLUS_GPIO_J8_18     = 24,  /*!< B+, Pin J8-18 */
        RPI_BPLUS_GPIO_J8_19     = 10,  /*!< B+, Pin J8-19, MOSI when SPI0 in use */
        RPI_BPLUS_GPIO_J8_21     =  9,  /*!< B+, Pin J8-21, MISO when SPI0 in use */
        RPI_BPLUS_GPIO_J8_22     = 25,  /*!< B+, Pin J8-22 */
        RPI_BPLUS_GPIO_J8_23     = 11,  /*!< B+, Pin J8-23, CLK when SPI0 in use */
        RPI_BPLUS_GPIO_J8_24     =  8,  /*!< B+, Pin J8-24, CE0 when SPI0 in use */
        RPI_BPLUS_GPIO_J8_26     =  7,  /*!< B+, Pin J8-26, CE1 when SPI0 in use */
        RPI_BPLUS_GPIO_J8_29     =  5,  /*!< B+, Pin J8-29,  */
        RPI_BPLUS_GPIO_J8_31     =  6,  /*!< B+, Pin J8-31,  */
        RPI_BPLUS_GPIO_J8_32     =  12, /*!< B+, Pin J8-32,  */
        RPI_BPLUS_GPIO_J8_33     =  13, /*!< B+, Pin J8-33,  */
        RPI_BPLUS_GPIO_J8_35     =  19, /*!< B+, Pin J8-35,  */
        RPI_BPLUS_GPIO_J8_36     =  16, /*!< B+, Pin J8-36,  */
        RPI_BPLUS_GPIO_J8_37     =  26, /*!< B+, Pin J8-37,  */
        RPI_BPLUS_GPIO_J8_38     =  20, /*!< B+, Pin J8-38,  */
        RPI_BPLUS_GPIO_J8_40     =  21  /*!< B+, Pin J8-40,  */
    } RPiGPIOPin;

    enum PinState {Low = 0, High = 1};
    enum PinMode {Input =0, Output = 1,  Alt0  = 0x04,
                  Alt1  = 0x05, Alt2  = 0x06, Alt3  = 0x07,
                  Alt4  = 0x03, Alt5  = 0x02};
    enum PinPud { Off   = 0x00, Down  = 0x01, Up      = 0x02};

    GPIO(GPIO::RPiGPIOPin  pinNum);
    virtual ~GPIO();

    void high(const TimeVal& keep = TimeVal::Zero);
    void low(const TimeVal& keep = TimeVal::Zero);
    virtual void set(PinState val, const TimeVal& keep = TimeVal::Zero) =0;
    virtual void trigger(const TimeVal& keep = TimeVal::Zero);

    bool isHigh();
    bool isLow();
    virtual PinState state() =0;

    void setInput();
    void setOutput();
    virtual void setMode (PinMode mode) =0;

    void pudUp ();
    void pudDown();
    virtual void setPud(PinPud pud) =0;

    TimeVal waitPin(PinState waitFor, const TimeVal& timeout, PinState &st);
    TimeVal waitPin(PinState waitFor, const TimeVal& timeout);
    TimeVal waitPin(PinState waitFor);

    TimeVal waitPinChange(const TimeVal& timeout, PinState &st);
    TimeVal waitPinChange(const TimeVal& timeout);
    TimeVal waitPinChange();
protected:
    GPIO::RPiGPIOPin  pinNum;

};

typedef std::unique_ptr<GPIO> GPIOPtr;

GPIO *getGPIODriver (GPIO::RPiGPIOPin pinNum);

#endif // GPIO_H
