#include "drivers/i2c.h"
#include "tools/tools.h"
#ifdef I2C_RAW_DATA
#   include <iostream>
#   include <iomanip>
#endif // I2C_RAW_DATA


#include "drivers/backend/bcm2835/i2c/bcm2835_i2c.h"
#include "drivers/backend/smbus/i2c/smbus_i2c.h"
#include "drivers/backend/wiringPi/i2c/wiringpi_i2c.h"

I2C* getI2CDriver (uint8_t deviceAddr)
{
#if defined(USE_WIRINGPI_FOR_I2C)
    return new I2C_wiringPi(deviceAddr);
#elif defined(USE_SMBUS_FOR_I2C)
    return new I2C_smbus("/dev/i2c-1", deviceAddr);
#elif defined(USE_BCM2835_FOR_I2C)
    return new I2C_bcm2835lib(deviceAddr);
#else
#   error "Define either bcm2835 or wiringPi or smbus backend for I2C"
#endif
}

void I2C::reg_set8(uint8_t reg, uint8_t data)
{
    write8(reg);
    write8(data);
}

uint8_t I2C::reg_get8 (uint8_t reg)
{
    write8(reg);
    return read8();
}

void I2C::reg_set16(uint8_t reg, uint16_t data)
{
    write8(reg);
    write16(data);
}

uint16_t I2C::reg_get16 (uint8_t reg)
{
    write8(reg);
    return read16();
}
