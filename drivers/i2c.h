#ifndef I2C_RW_H_INCLUDED
#define I2C_RW_H_INCLUDED

//#define I2C_RAW_DATA

#include <stdint.h>
#include <memory>

class I2C {
protected:
    uint8_t deviceAddr;
public:
    I2C (uint8_t deviceAddr):deviceAddr(deviceAddr){}
    virtual ~I2C() {}
    virtual void     write8 (uint8_t byte) =0;
    virtual uint8_t  read8 () =0;
    virtual void     write16(uint16_t word) =0;
    virtual uint16_t read16 () =0;

    virtual void     reg_set8 (uint8_t reg, uint8_t data);
    virtual uint8_t  reg_get8 (uint8_t reg);
    virtual void     reg_set16 (uint8_t reg, uint16_t data);
    virtual uint16_t reg_get16 (uint8_t reg);

    virtual size_t   reg_get_block(uint8_t reg, char* buf, int len) =0;
};

typedef std::unique_ptr<I2C> I2CPtr;

I2C* getI2CDriver (uint8_t deviceAddr);

#endif // I2C_RW_H_INCLUDED
