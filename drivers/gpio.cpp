#include "gpio.h"
#include "tools/time_util.h"
#include "tools/tools.h"

#include <stdexcept>

// GPIO back-ends
#include "drivers/backend/bcm2835/gpio/bcm2835_gpio.h"
#include "drivers/backend/wiringPi/gpio/wiringpi_gpio.h"

GPIO *getGPIODriver(GPIO::RPiGPIOPin pinNum)
{
#if defined(USE_WIRINGPI_FOR_GPIO)
    return new GPIO_wiringPi(pinNum);
#elif defined(USE_BCM2835_FOR_GPIO)
    return new GPIO_bcm2835lib(pinNum);
#else
#   error "Define either bcm2835 or wiringPi backend for GPIO"
#endif
}

GPIO::GPIO(RPiGPIOPin pinNum)
    :pinNum(pinNum)
{

}

GPIO::~GPIO()
{

}

void GPIO::high(const TimeVal &keep) { set (High, keep);}

void GPIO::low(const TimeVal &keep)  { set (Low, keep);}

void GPIO::set(GPIO::PinState val, const TimeVal &keep)
{
    rpi_delay(keep);
}

void GPIO::trigger(const TimeVal &keep) {set(isHigh()?Low:High, keep);}

bool GPIO::isHigh() { return state();}

bool GPIO::isLow()  {return !state();}

void GPIO::setInput() {setMode(GPIO::Input);}

void GPIO::setOutput() {setMode(Output);}

void GPIO::pudUp() { setPud(Up);}

void GPIO::pudDown() {setPud(Down);}

TimeVal GPIO::waitPin(GPIO::PinState waitFor, const TimeVal& timeout, PinState& st)
{
    TimeVal _start;
    TimeVal _end;

    do
    {
        _end.setCurrent();
        if (!timeout.isZero() && timeout < _end - _start)
            throw std::overflow_error("wait pin timed out");
        st = state();
    } while (st != waitFor);

    return _end - _start;
}

TimeVal GPIO::waitPin(GPIO::PinState waitFor, const TimeVal &timeout)
{
    GPIO::PinState pinVal;
    return waitPin(waitFor, timeout, pinVal);
}

TimeVal GPIO::waitPin(GPIO::PinState waitFor)
{
    return waitPin(waitFor, TimeVal::Zero);
}

TimeVal GPIO::waitPinChange(const TimeVal& timeout, PinState& st)
{
    PinState pinVal = (state() == High)? Low : High;
    return waitPin(pinVal, timeout, st);
}

TimeVal GPIO::waitPinChange(const TimeVal &timeout)
{
    GPIO::PinState pinVal;
    return waitPinChange(timeout, pinVal);
}

TimeVal GPIO::waitPinChange()
{
    return waitPinChange(TimeVal::Zero);
}
