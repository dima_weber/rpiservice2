#include <iostream>
#include "tools/tools.h"
#include <cstring>
#include <signal.h>
#include <vector>
#include <system_error>

#include "dht.h"
#include "bmp.h"
#include "cb7.h"
#include "hc-sr04.h"
#include "bh1750.h"
#include "ds18b20.h"
#include "pcf8563.h"
#include "ads1115.h"

#include "uart.h"
#include "oled.h"

#include "tools/centered.hpp"
#include "version.h"

const GPIO::RPiGPIOPin RAIN_DO  = GPIO::RPI_V2_GPIO_P1_13;
const GPIO::RPiGPIOPin CO2_DO  = GPIO::RPI_V2_GPIO_P1_15;
//const GPIO::RPiGPIOPin TRIG     = GPIO::RPI_V2_GPIO_P1_16;
//const GPIO::RPiGPIOPin ECHO     = GPIO::RPI_V2_GPIO_P1_18;
const GPIO::RPiGPIOPin DHT22    = GPIO::RPI_V2_GPIO_P1_15;
const GPIO::RPiGPIOPin DS18B20  = GPIO::RPI_V2_GPIO_P1_11;

bool stopRequired = false;

static
void signal_handler (int signum)
{
    (void) signum;
    stopRequired = true;
}

typedef std::shared_ptr<AbstractSensor> SensorPtr;

template<typename T, typename... Args>
T* add_sensor(std::vector<SensorPtr>& vec, const std::string& name, Args&&... arg)
{
    try
    {
        std::cout << "adding sensor "<< name;
        SensorPtr sensor = std::make_shared<T>(name.c_str(), std::forward<Args>(arg)...);
        vec.push_back(sensor);
        std::cout << " ok" << std::endl;
        return dynamic_cast<T*>(sensor.get());
    }
    catch(const std::system_error& e)
    {
        std::cout << " fail: " << e.what() << std::endl;
    }

    return nullptr;
}

int main(int argc, char* argv[])
{

    std::ostream& output = std::cout;
//    Uart uart_buff(std::cout);
//    output.rdbuf(&uart_buff);

    (void) argc;
    (void) argv;
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    std::cout   << "RPiSensors " << AutoVersion::FULLVERSION_STRING << std::endl;

    std::vector<SensorPtr> sensors;


    add_sensor<PCF8563Sensor>(sensors, "pcf8563");
    BH1750Sensor* pSens =  add_sensor<BH1750Sensor>(sensors, "bh1750");
    add_sensor<BMP180>(sensors, "bmp180");
    add_sensor<ADS1115>(sensors,"ads1115");
    add_sensor<CB7Sensor>(sensors,"cb7", "rain", RAIN_DO);
    add_sensor<CB7Sensor>(sensors,"mq-135", "co2", CO2_DO);
//  add_sensor<HC_SR04Sensor(sensors, "hc-sr04", TRIG, ECHO);
    add_sensor<DHT22Sensor>(sensors, "dht22", DHT22);

    OneWire onewire(DS18B20);
    output  << "1wire parasite present: " << (onewire.isParasite()?"yes":"no") << std::endl;
    onewire.setAllAlarmRange(22, 26);
    for (const SensorPtr& sensor: onewire.devices())
        sensors.push_back(sensor);

//    if (pSens)
//    {
//        pSens->poll();
//        pSens->setSensetivity(255);
//        pSens->setMode(BH1750Sensor::UltraHiRes, BH1750Sensor::OneTime);
//    }

    //    OLED oled;


    auto printTableHeader = [&sensors, &output]()
    {
        output << std::endl;
        for (SensorPtr& sensor: sensors)
        {
            size_t paramSize = sensor->parameters().size();
            output  << std::setw(AbstractSensor::WIDTH * paramSize + strlen(AbstractSensor::DELIMITER) * (paramSize - 1)) << centered(sensor->name()) << AbstractSensor::DELIMITER;
        }

        output   << std::endl;
        for (SensorPtr& sensor: sensors)
        {
            for (std::string& type : sensor->parameters())
                output << std::setw(AbstractSensor::WIDTH ) << centered(type) << AbstractSensor::DELIMITER ;
        }

        output   << std::endl;
    };

    int lines = 0;
    while (!stopRequired)
    {
        if (lines++ % 60 == 0)
            printTableHeader();
        for (auto& sensor: sensors)
        {
            sensor->poll();
//          output << sensor->display(output);
            output << sensor->onlyValuesDisplay();
        }

//        std::vector<OneWire::DS18B20SensorPtr> alarmedDs = onewire.alarmed();
//        if (!alarmedDs.empty())
//        {
//            output   << "alarmed ds18b20: " << std::endl;
//            for (OneWire::DS18B20SensorPtr pSensor: alarmedDs)
//            {
//                output   << "\t";
//                pSensor->display();
//            }
//        }

        output   << std::endl;
        rpi_delay(1000);
    }

    output   << "Exit" << std::endl;

    return 0;
}
