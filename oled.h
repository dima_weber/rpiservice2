#ifndef OLED_H
#define OLED_H

#include "drivers/i2c.h"

class OLED
{
    I2CPtr i2c;

public:
    OLED();
    ~OLED();
};

#endif // OLED_H
