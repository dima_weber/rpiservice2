#ifndef HCSR04_H_INCLUDED
#define HCSR04_H_INCLUDED

#include "abstractsensor.h"

class HC_SR04Sensor : public GPIOAbstractSensor
{
public:
    HC_SR04Sensor(const char* sensorName, GPIO::RPiGPIOPin trig, GPIO::RPiGPIOPin echo);
    void setTemperature(int temp10) {temperature = temp10; }

protected:
    virtual bool cleanup() override;
    virtual bool init() override;
    virtual bool execute() override;

    GPIOPtr requestGPIO;
    int temperature;

    uint16_t getSoundVelosity() const;
};

#endif // HCSR04_H_INCLUDED
