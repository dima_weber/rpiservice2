#ifndef UART_H
#define UART_H

#include <stdint.h>
#include <streambuf>
#include <sstream>
#include <vector>

class Uart : public std::streambuf
{
public:
    explicit Uart(std::ostream& sink, std::size_t   buff_sz = 256);
    Uart(const Uart& copy) =delete;
    Uart& operator= (const Uart& copy) =delete;
    virtual ~Uart();

protected:
    Uart& send (const uint8_t* data, int len = -1);
    Uart& recv (uint8_t * data, int len=-1);
    bool dumpInternalBuffer();

private:
    int_type overflow(int_type ch);
    int sync();

    std::ostream& sink_stream;
    std::vector<char> internalBuffer;
    int uart_stream_fd;
};

#endif
