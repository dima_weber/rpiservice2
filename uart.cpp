#include <stdio.h>
#include <unistd.h>			//Used for UART
#include <fcntl.h>			//Used for UART
#include <termios.h>		//Used for UART
#include <string.h>
#include <stdexcept>

#include "uart.h"
//-------------------------
//----- SETUP USART 0 -----
//-------------------------
//At bootup, pins 8 and 10 are already set to UART0_TXD, UART0_RXD (ie the alt0 function) respectively

//OPEN THE UART
int open_uart()
{
    int uart0_filestream = -1;

    //The flags (defined in fcntl.h):
    //	Access modes (use 1 of these):
    //		O_RDONLY - Open for reading only.
    //		O_RDWR - Open for reading and writing.
    //		O_WRONLY - Open for writing only.
    //
    //	O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
    //											if there is no input immediately available (instead of blocking). Likewise, write requests can also return
    //											immediately with a failure status if the output can't be written immediately.
    //
    //	O_NOCTTY - When set and path identifies a terminal device, open() shall not cause the terminal device to become the controlling terminal for the process.
    uart0_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);		//Open in non blocking read/write mode
    if (uart0_filestream == -1)
    {
            //ERROR - CAN'T OPEN SERIAL PORT
            printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
    }

    //CONFIGURE THE UART
    //The flags (defined in /usr/include/termios.h - see http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
    //	Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
    //	CSIZE:- CS5, CS6, CS7, CS8
    //	CLOCAL - Ignore modem status lines
    //	CREAD - Enable receiver
    //	IGNPAR = Ignore characters with parity errors
    //	ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of line characters - don't use for bianry comms!)
    //	PARENB - Parity enable
    //	PARODD - Odd parity (else even)
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;		//<Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);

    return uart0_filestream;
}

//----- TX BYTES -----
int tx_bytes(int uart_fd, const uint8_t* data, int data_len)
{
    int count = -1;
    if (uart_fd != -1)
    {
        count = write(uart_fd, data, data_len);		//Filestream, bytes to write, number of bytes to write
        if (count < 0)
        {
            printf("UART TX error\n");
        }
    }
    return count;
}

//----- CHECK FOR ANY RX BYTES -----
int rx_bytes(int uart_fd, uint8_t * data, int data_len)
{
    int rx_length = -1;
    if (uart_fd != -1)
    {
        // Read up to 255 characters from the port if they are there
        //memset(data, 0, (256 < data_len) ?  256:data_len));
        rx_length = read(uart_fd, (void*)data, data_len);		//Filestream, buffer to store in, number of bytes to read (max)
        if (rx_length < 0)
        {
            //An error occured (will occur if there are no bytes)
        }
        else if (rx_length == 0)
        {
            //No data waiting
        }
        else
        {
            //Bytes received
            data[rx_length] = '\0';
 //           printf("%i bytes read : %s\n", rx_length, data);
        }
    }
    return rx_length;
}

//----- CLOSE THE UART -----
void close_uart(int uart_fd)
{
	close(uart_fd);
}


Uart::Uart(std::ostream &sink, std::size_t buff_sz)
    :sink_stream(sink), internalBuffer (buff_sz+1)
{
    uart_stream_fd = open_uart();
    sink_stream.clear();
    char* pbegin = &internalBuffer.front();
    setp(pbegin, pbegin + internalBuffer.size() -1);
}

Uart::~Uart()
{
    close_uart(uart_stream_fd);
}

Uart &Uart::send(const uint8_t* data, int len )
{
    int length = len;
    if (length < 0)
        length = strlen(reinterpret_cast<const char*>(data));
    if (tx_bytes(uart_stream_fd, data, length) < 0)
        throw std::runtime_error("fail to send data to uart");
    return *this;
}

bool Uart::dumpInternalBuffer()
{
    try
    {
        int n = pptr() - pbase();
        send(reinterpret_cast<const uint8_t*>(pbase()), n);
        pbump(-n);
    }
    catch(const std::runtime_error& err)
    {
        return false;
    }

    return true;
}

Uart::int_type Uart::overflow(int_type ch)
{
    if (sink_stream && ch != traits_type::eof())
    {
        *pptr() = ch;
        pbump(1);
        if (dumpInternalBuffer())
            return ch;
    }

    return traits_type::eof();
}

int Uart::sync()
{
    return dumpInternalBuffer() ? 0 : -1;
}

