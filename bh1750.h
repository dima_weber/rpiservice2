#ifndef BH1750_H_INCLUDED
#define BH1750_H_INCLUDED

#include <stdint.h>
#include "abstractsensor.h"

class BH1750Sensor : public I2CAbstractSensor
{
public:
    enum MeasureMode {LowRes = 0x03, HiRes =0x00, UltraHiRes=0x01};
    enum PeriodicMode {OneTime=0x20, Continous = 0x10};
    const uint8_t MeasureMask  = 0x03;
    const uint8_t PeriodMask = 0x30;

    BH1750Sensor(const char* sensorName);
    void setMode(MeasureMode mesMode = HiRes, PeriodicMode period = Continous);
    void setSensetivity(uint8_t sens );
    uint8_t sensetivity() const;

protected:
    virtual bool cleanup() override;
    virtual bool init() override;
    virtual bool execute();

        uint16_t measureDelay() const;
        uint8_t mode;
        uint8_t sens;
};

#endif // BH1750_H_INCLUDED
