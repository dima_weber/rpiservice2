#include "cb7.h"
#include <iostream>
#include <iomanip>

CB7Sensor::CB7Sensor(const char* sensorName, const std::string &valueName, GPIO::RPiGPIOPin pin)
    : GPIOAbstractSensor(sensorName, pin, 0), valName(valueName)
{
    // no measure delay
    registerValue(valName, SensorRecord::Rain);
}

bool CB7Sensor::cleanup()
{
    return GPIOAbstractSensor::cleanup();
}

bool CB7Sensor::init()
{
    gpio->setInput();
    gpio->pudUp();
    return GPIOAbstractSensor::init();
}

bool CB7Sensor::execute()
{
    GPIO::PinState rainStatus = gpio->state();
    setValue(valName,  rainStatus == GPIO::Low? 1 : 0);
    return true;
}
