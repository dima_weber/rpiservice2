#ifndef ABSTRACTSENSOR_H
#define ABSTRACTSENSOR_H

#include <memory>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <vector>
#include <map>

#include "drivers/i2c.h"
#include "drivers/gpio.h"

struct SensorRecord
{
    enum ValueType {Unknown, Number, Hex, Boolean, VDate, VTime, Percent};
    enum SensType {Temperature, Humidity, Pressure, Luminosity, Rain, Distance, Date, Time, General};
    SensType type;
    bool valid;
    union
    {
        int32_t value;
        std::tm datetime;
    };

    static const char* name(SensType type)
    {
        switch (type)
        {
            case Temperature: return "temperature";
            case Humidity: return "humidity";
            case Pressure: return "pressure";
            case Luminosity: return "luminosity";
            case Rain: return "rain";
            case Distance: return "distance";
            case Date:  return "date";
            case Time: return "time";
            case General: return "general";
            default: return "unknown";
        }
    }

    const char* unit() const
    {
        switch (type)
        {
            case Temperature: return "*C";
            case Humidity: return "%";
            case Pressure: return "hPa";
            case Luminosity: return "lux";
            case Distance: return "cm";
            case General:
            case Rain:
            case Time:
            case Date: return nullptr;
            default: return "";
        }
    }

    int precision() const
    {
        switch (type)
        {
            case Luminosity:
            case Pressure:
                           return 2;
            case Temperature:
            case Humidity:
                           return 1;
            case Distance:
                           return 0;
            case Rain:
            case Date:
            case Time:
            case General:
            default: return 0;
        }
    }

    ValueType valueType() const
    {
        switch (type)
        {
            case Pressure:
            case Temperature:
            case Humidity:
            case Luminosity:
            case Distance: return Number;
            case General: return Percent;
            case Rain: return Boolean;
            case Date: return VDate;
            case Time: return VTime;
            default: return Unknown;
        }
    }

    std::string valueToString() const
    {
        static const float scale[] = {1, 10, 100, 1000};
        std::stringstream  s;
        int p = precision();
        char formatedDate[64] = {0};
        if (!valid)
            s << "* ";
        switch (valueType())
        {
            case Number:
                s << std::fixed << std::setprecision(p) << value / scale[p] << " " << unit();
                break;
            case Percent:
                s << std::fixed << std::setprecision(2) << value / scale[2] << " " << unit();
                break;
            case Boolean:
                s << (value?"on":"off");
                break;
            case VDate:
                strftime(formatedDate, sizeof(formatedDate)-1, "%d %b %Y %a", &datetime);
                s << formatedDate;
                break;
            case VTime:
                strftime(formatedDate, sizeof(formatedDate)-1, "%T", &datetime);
                s << formatedDate;
                break;
            case Hex:
                s << std::setw(4) << std::hex << (value & 0x0000FFFF) << std::dec << " " << unit();
                break;
            default: s << "unknown";
        }
        return s.str();
    }
    std::string toString() const
    {
        std::stringstream  s;
        s << name(type) << "=" << valueToString();
        return s.str();
    }
};

typedef std::map<std::string, SensorRecord> PollResult;

std::ostream& operator<<(std::ostream& stream, const SensorRecord& rec);
std::ostream& operator<<(std::ostream& stream, const PollResult& rec);

class AbstractSensor
{
    public:
        static constexpr const char* DELIMITER  = " |";
        static constexpr char WIDTH = 15;

        AbstractSensor(const char* sensorName, uint16_t minimalDelayMs );
        virtual ~AbstractSensor();

        virtual bool poll();
        const PollResult& values() const { return valuesMap;}
        bool reset(bool force = false);
        const char* name() const {return sensorName.c_str();}
        std::string display() const;
        std::string onlyValuesDisplay () const;
        virtual std::vector<std::string> parameters() const;

    protected:
        void setValue(const std::string& resName, uint32_t value);
        void setValue(SensorRecord::SensType type, uint32_t value);
        void setValue(const std::string& resName, const std::tm& value);
        void setValue(SensorRecord::SensType type, const std::tm& value);
        virtual bool execute() =0;
        virtual bool init() =0;
        virtual bool cleanup(); // should be pure virtual, but we call it from destructor so have to implement
        std::string failReason;
        timeval minimalDelay;

        void registerValue(const std::string& resName, SensorRecord::SensType type);
        void registerValue(SensorRecord::SensType type);

    private:
        void invalidateValues();

        PollResult valuesMap;
        bool initialized;
        const std::string sensorName;
        timeval lastExecuteTime;
};

class GPIOAbstractSensor : public AbstractSensor
{
protected:
    GPIOPtr gpio;
public:
    GPIOAbstractSensor(const char* sensorName, GPIO::RPiGPIOPin pin, uint16_t minmalDelayMs);
};

class I2CAbstractSensor : public AbstractSensor
{

public:
    I2CAbstractSensor(const char* sensorName, uint8_t deviceAddr, uint16_t minmalDelayMs );
    virtual ~I2CAbstractSensor();

    virtual bool poll() override;

protected:
    I2CPtr i2c;
};
#endif // ABSTRACTSENSOR_H
