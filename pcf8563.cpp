#include "pcf8563.h"
#include "drivers/i2c.h"
#include "tools/tools.h"
#include <ctime>

#define PCF8563_I2C_ADDRESS 0x51
#define PCF8563_REG_SECOND  0x02
#define PCF8563_REG_MINUTE  0x03
#define PCF8563_REG_HOUR    0x04
#define PCF8563_REG_DAY     0x05
#define PCF8563_REG_WEEKDAY 0x06
#define PCF8563_REG_MONTH   0x07
#define PCF8563_REG_YEAR    0x08
#define PCF8563_REG_CLKOUT  0x0D


#define PCF8563_SECOND_MASK 0x7F
#define PCF8563_MINUTE_MASK 0x7F
#define PCF8563_HOUR_MASK   0x3F
#define PCF8563_DAY_MASK    0x3F
#define PCF8563_WEEKDAY_MASK   0x07
#define PCF8563_MONTH_MASK   0x1F
#define PCF8563_YEAR_MASK   0xFF
#define PCF8563_CENTURY_MASK   0x80

#define PCF8563_FREQ_32KHZ      0x00
#define PCF8563_FREQ_1024KHZ    0x01
#define PCF8563_FREQ_32HZ       0x10
#define PCF8563_FREQ_1HZ        0x11
#define PCF8563_CLK_ENABLE      0x80

PCF8563Sensor::PCF8563Sensor(const char* sensorName)
    : I2CAbstractSensor(sensorName, PCF8563_I2C_ADDRESS, 1)
{
    registerValue(SensorRecord::Date);
    registerValue(SensorRecord::Time);
}

bool PCF8563Sensor::init()
{
    i2c->reg_set8(PCF8563_REG_CLKOUT, PCF8563_CLK_ENABLE | PCF8563_FREQ_1024KHZ);
    return I2CAbstractSensor::init();
}

bool PCF8563Sensor::cleanup()
{
    i2c->reg_set8(PCF8563_REG_CLKOUT, 0);
    return I2CAbstractSensor::cleanup();
}

bool PCF8563Sensor::execute()
{
    uint8_t byte = 0;
    bool integrityGuaranteed;
    std::tm datetime;
    bool centuryLeap;

    byte = i2c->reg_get8(PCF8563_REG_SECOND);
    datetime.tm_sec = bcd_to_dec(byte & PCF8563_SECOND_MASK);
    integrityGuaranteed = (byte & !PCF8563_SECOND_MASK) == 0;

    byte = i2c->reg_get8(PCF8563_REG_MINUTE);
    datetime.tm_min = bcd_to_dec(byte & PCF8563_MINUTE_MASK);

    byte = i2c->reg_get8(PCF8563_REG_HOUR);
    datetime.tm_hour = bcd_to_dec(byte & PCF8563_HOUR_MASK);

    byte = i2c->reg_get8(PCF8563_REG_DAY);
    datetime.tm_mday = bcd_to_dec(byte & PCF8563_DAY_MASK);

    byte = i2c->reg_get8(PCF8563_REG_WEEKDAY);
    datetime.tm_wday = bcd_to_dec(byte & PCF8563_WEEKDAY_MASK);

    byte = i2c->reg_get8(PCF8563_REG_MONTH);
    datetime.tm_mon = bcd_to_dec(byte & PCF8563_MONTH_MASK) -1;
    centuryLeap = bcd_to_dec(byte & PCF8563_CENTURY_MASK);

    byte = i2c->reg_get8(PCF8563_REG_YEAR);
    datetime.tm_year = (centuryLeap?200:100) + bcd_to_dec(byte & PCF8563_YEAR_MASK) ;

    datetime.tm_yday = 0;
    datetime.tm_isdst = -1;

    if (checkDatetime(datetime))
    {
        setValue(SensorRecord::Date, datetime);
        setValue(SensorRecord::Time, datetime);
    }

    return integrityGuaranteed;
}

bool PCF8563Sensor::checkDatetime(const std::tm& datetime)
{
    return     datetime.tm_sec >= 0 && datetime.tm_sec <= 60
            && datetime.tm_min >= 0 && datetime.tm_min < 60
            && datetime.tm_hour >= 0 && datetime.tm_hour < 24
            && datetime.tm_mday >= 0 && datetime.tm_mday < 31
            && datetime.tm_wday >= 0 && datetime.tm_wday < 7
            && datetime.tm_mon >= 0 && datetime.tm_mon < 12
            && datetime.tm_year >= 100 && datetime.tm_year < 300;
}
