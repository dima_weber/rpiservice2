#include "ds18b20.h"
#include "tools/tools.h"
#include <stdexcept>
#include <utility>

#define DS18B20_SKIP_ROM            0xCC
#define DS18B20_SEARCH_ROM          0xF0
#define DS18B20_READ_ROM            0x33
#define DS18B20_MATCH_ROM           0x55

#define DS18B20_CONVERT_T           0x44

#define DS18B20_READ_SCRATCHPAD     0xBE
#define DS18B20_WRITE_SCRATCHPAD    0x4E
#define DS18B20_COPY_SCRATCHPAD     0x48
#define DS18B20_RECALL_E2           0xB8

#define DS18B20_ALARM_SEARCH        0xEC
#define DS18B20_READ_POWER_SUPPLY   0xB4


#define DS18B20_SCRATCHPAD_SIZE 9
#define DS18B20_SCRATCHPAD_TEMP_LSB_ADDR   0
#define DS18B20_SCRATCHPAD_TEMP_MSB_ADDR   1
#define DS18B20_SCRATCHPAD_TH_ADDR   2
#define DS18B20_SCRATCHPAD_TL_ADDR   3
#define DS18B20_SCRATCHPAD_COUNT_REMAIN_ADDR   6
#define DS18B20_SCRATCHPAD_COUNT_PER_C_ADDR   7
#define DS18B20_SCRATCHPAD_CRC_ADDR   8


OneWire::OneWire (GPIO::RPiGPIOPin pin)
    :discovered_1wire_Id(), discovered_devices(), gpio(getGPIODriver(pin))
{
    if (gpio->isLow())
    {
        // throw std::runtime_error("No 1-wire devices");
        return;
    }
    search_devices(DS18B20_SEARCH_ROM, discovered_1wire_Id);
    std::cout << "discovered ids:";
    for(DeviceID id : discovered_1wire_Id)
    {
        std::stringstream os;
        os << "ds-" << std::hex << std::setw(16) << std::setfill('0') << id.to_ullong();
        std::cout << os.str() << " ";
        DS18B20SensorPtr pSensor (new DS18B20Sensor(os.str().c_str(), *this, id));
        auto insertItem = std::pair<DeviceID, DS18B20SensorPtr>(id, pSensor);
        discovered_devices.insert(discovered_devices.begin(), insertItem);
    }
    reset();
    std::cout << std::dec << std::endl;

}

void OneWire::write_1_time_slot()
{
    gpio->setOutput();
    gpio->low();
    rpi_delayMicroseconds(2);
    gpio->setInput();
    rpi_delayMicroseconds(58);
}

void OneWire::write_0_time_slot()
{
    gpio->setOutput();
    gpio->low();
    rpi_delayMicroseconds(2);
    rpi_delayMicroseconds(58);
    gpio->setInput();
    rpi_delayMicroseconds(1);
}

bool OneWire::read_time_slot()
{
    gpio->setOutput();
    gpio->low();
    rpi_delayMicroseconds(1);
    gpio->setInput();
    rpi_delayMicroseconds(13);
    bool ret = gpio->state() == GPIO::High;
    rpi_delayMicroseconds(47);
    return ret;
}

bool OneWire::reset()
{
    gpio->setOutput();

    gpio->low();
    rpi_delayMicroseconds(480);
    gpio->setInput();
    rpi_delayMicroseconds(60);

    bool dsPresent =  gpio->state() == GPIO::Low;

    rpi_delayMicroseconds(480);

    return dsPresent;
}

uint8_t OneWire::read8 ()
{
    uint8_t result = 0;
    for (int i=0; i<8; i++)
        result |=  read_time_slot() << i;
    return result;
}

void OneWire::write8(uint8_t cmd)
{
    for (int i=0;i < 8; i++)
    {
        if (cmd & (1 << i))
            write_1_time_slot();
        else
            write_0_time_slot();
    }
}

void OneWire::setAllLowAlarm(int8_t lowTemp)
{
    for (auto pair: discovered_devices)
    {
        pair.second->setLowAlarm(lowTemp);
    }
}

void OneWire::setAllHighAlarm(int8_t highTemp)
{
    for (auto pair: discovered_devices)
    {
        pair.second->setHighAlarm(highTemp);
    }
}

void OneWire::setAllAlarmRange(int8_t lowTemp, int8_t highTemp)
{
    std::cout << "set ds alarms" << std::endl;
    for (auto pair: discovered_devices)
    {
        pair.second->setAlarmRange(lowTemp, highTemp);
    }
}

std::vector<OneWire::DS18B20SensorPtr> OneWire::alarmed()
{
    std::vector<DeviceID> alarmedDeviceId;
    std::vector<OneWire::DS18B20SensorPtr> alarmedDevices;

//    std::cout << "search alarmed" << std::endl;
    reset();
    search_devices(DS18B20_ALARM_SEARCH, alarmedDeviceId);
    reset();

    for (DeviceID id: alarmedDeviceId)
    {
//        std::cout << "alarmed device" << std::endl;
        alarmedDevices.push_back( discovered_devices[id]);
    }
    return alarmedDevices;
}

bool OneWire::isParasite()
{
    reset();
    write8(DS18B20_READ_POWER_SUPPLY);
//    During the read-time slot, parasite powered DS18S20s will pull the bus low,
//       and externally powered DS18S20s will let the bus remain high
    bool isParasites = read_time_slot() == false;
    return isParasites;
}


void OneWire::search_devices(uint8_t searchCmd, std::vector<DeviceID>& discoveredId, const DeviceID& partial_id, size_t partial_size)
{
    DeviceID deviceId = partial_id;
    DeviceID nextId =0;
    size_t nextlen =0;
    uint8_t bit = 0;
    bool answer =0;

    if (!reset())
    {
        // throw std::runtime_error("No 1-wire devices");
        return;
    }

    write8(searchCmd);
    for (size_t i=0; i<deviceId.size(); i++)
    {
        bit = read_time_slot()?1:0;
        bit <<= 1;
        bit |= read_time_slot()?1:0;
        if (i <= partial_size)
        {
//            std::cout << (deviceId[i]  ? '1' : '0');
            answer = deviceId[i] ? 1:0 ;
        }
        else
        {
            if (bit == 0x02)
            {
//                std::cout << (int)1;
                deviceId.set(i, 1);
                answer = true;
            }
            else if (bit == 0x01)
            {
//                std::cout << (int)0;
                //deviceId[i].reset();
                answer = false;
            }
            else if (bit == 0x00)
            {
//                std::cout << '*';
                answer = false;
                if (nextlen == 0)
                {
                    nextId = deviceId;
                    nextId.set(i, 1);
                    nextlen = i;
                }
            }
            else
            {
                // throw std::runtime_error("rom 2-bit recieved 0x11 -- impossible");
                //std::cout << "?";
                if (searchCmd == DS18B20_ALARM_SEARCH)
                {
//                    std::cout << "no alarms" << std::endl;
//                    return;
                }
                break;
            }
        }
//        std::cout <<  (answer ? "+" : "-");
        answer ? write_1_time_slot() : write_0_time_slot();
    }
    reset();

    uint8_t crc_accumulator = 0;
    uint64_t num = deviceId.to_ullong();
    uint8_t* p = reinterpret_cast<uint8_t*>(&num);
    for (int i=0; i < 8; i++)
    {
        uint8_t byte = * (p + i);
//        std::cout << "push " << std::hex << std::setw(2) << (int) byte << " ";
        crc_accumulator = crc8( byte, crc_accumulator );
    }
    if (crc_accumulator != 0 || num == 0)
    {
//        std::cout << "ROM checksum fail. Discover again" << std::endl;
        nextlen = 0;
        nextId = 0;
        search_devices(searchCmd, discoveredId, partial_id, partial_size);
    }
    else
    {
//        std::cout << "ROM check OK. Push to discovered id list" << std::endl;
        discoveredId.push_back(deviceId);
    }

    if (nextlen > 0)
    {
//        std::cout << "another device(s) present. search again" << std::endl;
//        std::cout << "partial len:" << nextlen << std::endl;
        search_devices(searchCmd, discoveredId, nextId, nextlen);
    }
}

void OneWire::send_rom (const DeviceID& id)
{
    uint64_t num = id.to_ullong();
    uint8_t* p = reinterpret_cast<uint8_t*>(&num);
    uint8_t byte =0;
    for(size_t i=0; i< sizeof(num); i++)
    {
        byte = *(p + i);
        write8(byte);
//        std::cout << "send " << std::hex << std::setw(2) << (int)byte << " ";
    }
}

DS18B20Sensor::DS18B20Sensor(const char* sensorName, OneWire& onewire, OneWire::DeviceID id)
    :AbstractSensor(sensorName, 800), onewire(onewire), deviceId(id)
{
    // convertion time 750 ms
    registerValue(SensorRecord::Temperature);
}

bool DS18B20Sensor::execute()
{
    /// TODO: when more then 1 wire present -- CONVERT_T should be called only once.
    reset();
    onewire.write8(DS18B20_SKIP_ROM);
    onewire.write8(DS18B20_CONVERT_T);

    // for parasite power -- delay only applied, for external power -- readtime_slot while 0
    if (onewire.isParasite())
    {
        rpi_delay(10);
    }
    else
        while(onewire.read_time_slot()==0)
            ;

    int retries = 10;
    do
    {
        onewire.reset();
        onewire.write8(DS18B20_MATCH_ROM);
        onewire.send_rom(deviceId);
        onewire.write8(DS18B20_READ_SCRATCHPAD);

        uint8_t byte[DS18B20_SCRATCHPAD_SIZE];
        for (size_t i=0; i< sizeof(byte); i++)
        {
            byte[i] = onewire.read8();
        }
        onewire.reset();

        uint8_t crc_accumulator = 0;
        for (size_t i=0; i<sizeof(byte); i++)
            crc8(byte[i], crc_accumulator);

        if (crc_accumulator != 0)
        {
            retries--;
        }
        else
        {
            uint16_t temp = byte[DS18B20_SCRATCHPAD_TEMP_MSB_ADDR] * 256 + byte[DS18B20_SCRATCHPAD_TEMP_LSB_ADDR];

            uint8_t count_remain = byte[DS18B20_SCRATCHPAD_COUNT_REMAIN_ADDR];
            uint8_t count_per_C = byte[DS18B20_SCRATCHPAD_COUNT_PER_C_ADDR];
            if (count_per_C == 0)
                throw std::runtime_error("fail to read data");

            scratchpad_th = byte[DS18B20_SCRATCHPAD_TH_ADDR];
            scratchpad_tl = byte[DS18B20_SCRATCHPAD_TL_ADDR];

//            for(int i=0; i<sizeof(byte); i++)
//                std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)byte[i] << " ";
//            std::cout << std::dec << std::endl;

            temp = temp*10 / count_per_C;
            //addResult(SensorRecord::Temperature, temp);
            setValue(SensorRecord::Temperature, temp + 10 * (count_per_C - count_remain) / count_per_C) ;

            retries = -1;
        }
    } while (retries > 0);
    if (retries == 0)
        throw  std::runtime_error("checksum fail");
    return retries == -1;
}

void DS18B20Sensor::setLowAlarm(int8_t temp)
{
    setAlarmRange(temp, scratchpad_th);
}

void DS18B20Sensor::setHighAlarm(int8_t temp)
{
    setAlarmRange(scratchpad_tl, temp);
}

void DS18B20Sensor::setAlarmRange(int8_t low, int8_t high)
{
//    std::cout << name() << " write alarm values "
//              << std::hex << std::setw(2) << std::setfill('0')
//              << (int)low << " " << (int)high
//              << std::endl;
    scratchpad_th = high; // internally data stored in 0.5 grade
    scratchpad_tl = low;

    onewire.reset();
    onewire.write8(DS18B20_MATCH_ROM);
    onewire.send_rom(deviceId);
    onewire.write8(DS18B20_WRITE_SCRATCHPAD);
    onewire.write8(scratchpad_th);
    onewire.write8(scratchpad_tl);
//    rpi_delay(1);

    onewire.reset();
//    std::cout << name() << "save to eeprom" << std::endl;
    onewire.write8(DS18B20_MATCH_ROM);
    onewire.send_rom(deviceId);
    onewire.write8(DS18B20_COPY_SCRATCHPAD);
    // wait while copying ready
    if (onewire.isParasite())
        rpi_delay(10);
    else
        while (!onewire.read_time_slot())
            ;

    onewire.reset();
//    std::cout << name() << " checkvalues" << std::endl;
    onewire.write8(DS18B20_MATCH_ROM);
    onewire.send_rom(deviceId);
    onewire.write8(DS18B20_READ_SCRATCHPAD);

    uint8_t byte[DS18B20_SCRATCHPAD_SIZE];
    for (size_t i=0; i< sizeof(byte); i++)
    {
        byte[i] = onewire.read8();
    }
    if (   byte[DS18B20_SCRATCHPAD_TL_ADDR] != scratchpad_tl
        || byte[DS18B20_SCRATCHPAD_TH_ADDR] != scratchpad_th)
    {
//        std::cout << std::hex << std::setw(2) << std::setfill('0')
//                  << "\t high w: " << (int)scratchpad_th << " r: " <<  (int)byte[DS18B20_SCRATCHPAD_TH_ADDR]
//                  << std::endl
//                  << "\t  low w: " << (int)scratchpad_th << " r: " <<  (int)byte[DS18B20_SCRATCHPAD_TL_ADDR]
//                  << std::dec << std::endl;
        std::cout << "write alarm values fail" << std::endl;
    }
    onewire.reset();
}


bool DS18B20Sensor::init()
{
    return  AbstractSensor::init();
}

bool DS18B20Sensor::cleanup()
{
    return AbstractSensor::cleanup();
}
