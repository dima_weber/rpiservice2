#ifndef PCF8563_H
#define PCF8563_H

#include "abstractsensor.h"
namespace std {
    struct tm;
}

class PCF8563Sensor : public I2CAbstractSensor
{
    /// TODO: add timer support
    /// TODO: add write support
public:
    PCF8563Sensor(const char* sensorName);

protected:
    virtual bool init() override;
    virtual bool cleanup() override;
    virtual bool execute() override;

    static bool checkDatetime(const struct  std::tm& datetime);
};
#endif // PC8563_H
