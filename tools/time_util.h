#ifndef TIME_UTIL_H
#define TIME_UTIL_H

#include <iostream>
#include <sys/time.h>
#include <iomanip>

std::ostream& operator << (std::ostream& stream, const  timeval& tv);

bool operator < (const struct timeval& a, const struct timeval& b);
bool operator > (const struct timeval& a, const struct timeval& b);
bool operator >= (const struct timeval& a, const struct timeval& b);
bool operator <= (const struct timeval& a, const struct timeval& b);
bool operator != (const struct timeval& a, const struct timeval& b);
bool operator == (const struct timeval& a, const struct timeval& b);

struct timeval operator - (const struct timeval& a, const struct timeval& b);
struct timeval operator + (const struct timeval& a, const struct timeval& b);

class TimeVal
{
    struct timeval t;

public:
    static const TimeVal Zero;
    TimeVal(uint32_t usec);
    TimeVal(uint32_t sec, uint32_t usec);
    TimeVal();
    TimeVal(const timeval& other);

    operator struct timeval() const;
    void setCurrent();
    bool isZero() const;
    friend void rpi_delay(const TimeVal &tv);
};

class ElapsReport
{
    TimeVal timer_start;
    TimeVal asked;

public:
    ElapsReport();
    ElapsReport(const TimeVal& ask);
    ElapsReport(uint32_t sec, uint32_t usec);
    ~ElapsReport();
};

#endif
