#include "tools.h"

#if defined(USE_BCM2835_FOR_GPIO)
#   include <bcm2835.h>
#elif  defined(USE_WIRINGPI_FOR_GPIO)
#   include <wiringPi.h>
#else
#   error "Define either bcm2835 or wiringPi backend"
#endif

void rpi_delay(int ms)
{
#ifdef SHOW_DELAY
    ElapsReport el(0, ms * 1000);
#endif
    delay(ms);
}

void rpi_delayMicroseconds(int us)
{
#ifdef SHOW_DELAY
    ElapsReport el(0, us);
#endif
    delayMicroseconds(us);
}

uint8_t bcd_to_dec (uint8_t bcd)
{
    return (((bcd & 0xF0) >> 4) % 10) * 10 + (bcd & 0x0F ) % 10;
}


void rpi_delay(const TimeVal &tv)
{
#ifdef SHOW_DELAY
    ElapsReport el(tv);
#endif
    if (tv.t.tv_sec)
        delay(tv.t.tv_sec);
    if (tv.t.tv_usec)
        delayMicroseconds(tv.t.tv_usec);
}

uint8_t crc8 (int data, uint8_t& crc_accumulator)
{
    uint8_t tmp1;

    for (uint8_t bit_cnt = 8; bit_cnt != 0; bit_cnt--)
    {
        tmp1 =(data ^ crc_accumulator) &1;
        data >>=1;       /* shift right one bit */
        crc_accumulator >>=1;       /* shift right one bit */
        if (tmp1 !=0)
            crc_accumulator ^=0x8c;      /* x8+x5+x4+1 shifted one bit right */
     }
    return (crc_accumulator);
}
