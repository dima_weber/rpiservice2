#include <ctime>
#include <sys/time.h>
#include <iostream>
#include "time_util.h"
#include "tools.h"

#define SYSTEM_TIMEVAL

std::ostream& operator << (std::ostream& stream, const timeval& tv)
{
    stream << tv.tv_sec << '.' << tv.tv_usec/1000 << '.' << tv.tv_usec%1000;
    return stream;
}

bool operator < (const struct timeval& a, const struct timeval& b)
{
#ifdef SYSTEM_TIMEVAL
    return timercmp(&a, &b, <) != 0;
#else
    return a.tv_sec < b.tv_sec || a.tv_sec == b.tv_sec && a.tv_usec < b.tv_usec;
#endif
}

bool operator == (const struct timeval& a, const struct timeval& b)
{
#ifdef SYSTEM_TIMEVAL
    return timercmp(&a, &b, ==) != 0;
#else
    return !(a<b || b<a);
#endif
}

bool operator != (const struct timeval& a, const struct timeval& b)
{
#ifdef SYSTEM_TIMEVAL
    return timercmp(&a, &b, !=) != 0;
#else
    return (a<b || b<a);
#endif
}

bool operator <= (const struct timeval& a, const struct timeval& b)
{
#ifdef SYSTEM_TIMEVAL
    return timercmp(&a, &b, <=) != 0;
#else
    return (a<b || a==b);
#endif
}

bool operator > (const struct timeval& a, const struct timeval& b)
{
#ifdef SYSTEM_TIMEVAL
    return timercmp(&a, &b, >) != 0;
#else
    return !(a <= b);
#endif
}

bool operator >= (const struct timeval& a, const struct timeval& b)
{
#ifdef SYSTEM_TIMEVAL
    return timercmp(&a, &b, >=) != 0;
#else
    return !(a < b);
#endif
}

struct timeval operator- (const struct timeval& a, const struct timeval& b)
{
    struct timeval sub;
#ifdef SYSTEM_TIMEVAL
    timersub(&a, &b, &sub);
#else
    sub.tv_sec = a.tv_sec - b.tv_sec;
    sub.tv_usec = a.tv_usec - b.tv_usec;
    if (sub.tv_usec < 0)
    {
        sub.tv_sec--;
        sub.tv_usec += 1000000;
    }
#endif
    return sub;
}

struct timeval operator+ (const struct timeval& a, const struct timeval& b)
{
    struct timeval sub;
#ifdef SYSTEM_TIMEVAL
    timeradd(&a, &b, &sub);
#else
    sub.tv_sec = a.tv_sec + b.tv_sec;
    sub.tv_usec = a.tv_usec + b.tv_usec;
    if (sub.tv_usec > 1000000)
    {
        sub.tv_sec++;
        sub.tv_usec -= 1000000;
    }
#endif
    return sub;
}

const TimeVal TimeVal::Zero (0,0);


ElapsReport::ElapsReport()
    :timer_start(), asked(TimeVal::Zero)
{
}

ElapsReport::ElapsReport(const TimeVal &ask)
    :timer_start(), asked(ask)
{
}

ElapsReport::ElapsReport(uint32_t sec, uint32_t usec)
    :timer_start(), asked(sec, usec)
{
}

ElapsReport::~ElapsReport()
{
    struct timeval v = TimeVal() - timer_start ;
    struct timeval s = asked;

    std::cout << "delay: " << v;
    if (asked != TimeVal::Zero)
    {
        std::cout << " (asked " << asked << "), " << std::fixed << std::setprecision(2) << (v.tv_usec * 100.0) / s.tv_usec << "%";
    }
    std::cout << std::endl;
}


TimeVal::operator timeval() const
{ return t;}

TimeVal::TimeVal(uint32_t usec)
{
    t.tv_sec = usec / 1000000;
    t.tv_usec = usec % 1000000;
}

TimeVal::TimeVal(uint32_t sec, uint32_t usec)
{
    t.tv_sec = sec;
    t.tv_usec = usec;
}

TimeVal::TimeVal()
{
    setCurrent();
}

TimeVal::TimeVal(const timeval &other)
    :t(other)
{}

void TimeVal::setCurrent()
{
    struct timezone tz;
    gettimeofday(&t, &tz);
}

bool TimeVal::isZero() const
{
    return t == TimeVal::Zero.t;
}
