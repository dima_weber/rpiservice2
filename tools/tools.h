#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

//#define SHOW_DELAY
#include "time_util.h"

void rpi_delay(int ms);
void rpi_delayMicroseconds(int us);
void rpi_delay(const TimeVal& tv);

uint8_t bcd_to_dec (uint8_t bcd);
uint8_t crc8 (int data, uint8_t& crc_accumulator);

static uint16_t revert16 (uint16_t a)
{ return ((a & 0x00FF) << 8) | ((a & 0xFF00) >> 8);}

#endif // TOOLS_H_INCLUDED
