#include <ctime>
#include "dht.h"
#include "tools/tools.h"
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/time.h> // for timersub() / struct timeval
#include <cstring> // for memset
#include <stddef.h>
#include <sys/mman.h> // for mem lock
#include <sched.h>
#include <unistd.h>
#include <system_error>
#include <errno.h>

#define DHT_DATA_BITS            40
#define DHT_WAKEUP_DELAY_MS      30
#define DHT_INITIALIZE_DELAY_MS  2
#define DHT_TIMEOUT_US           100
#define DHT_DATA_BIT1_DELAY_US   60

DHT22Sensor::DHT22Sensor(const char* sensorName, GPIO::RPiGPIOPin pin)
    :GPIOAbstractSensor(sensorName, pin, 2000)
{
    // minimal collecting period 1.7 sec
    // maximal 2 sec
    registerValue(SensorRecord::Temperature);
    registerValue(SensorRecord::Humidity);
}

bool DHT22Sensor::cleanup()
{
    GPIOAbstractSensor::cleanup();
    int ret = munlockall();
    if (ret)
        throw std::system_error(errno, std::system_category(), "fail to unlock memory");
    return ret == 0;
}

bool DHT22Sensor::init()
{
    GPIOAbstractSensor::init();
    int ret = mlockall(MCL_CURRENT | MCL_FUTURE);
    if (ret)
        throw std::system_error(errno, std::system_category(), "fail to unlock memory");
    return ret == 0;
}

class TimeCriticalLock
{
public:
    TimeCriticalLock(int desiredPolicy)
        :prioritySwitched(false), currentSchedPolicy(0), currentSchedParam({0}), desiredSchedPolicy(desiredPolicy)
    {
        time_critical_begin();
    }
    ~TimeCriticalLock()
    {
        time_critical_end();
    }

private:
    bool prioritySwitched;
    int currentSchedPolicy;
    sched_param currentSchedParam;
    int desiredSchedPolicy;

    void time_critical_begin()
    {
        pid_t pid = 0; // this thread

        try {
    #ifdef MEMLOCK
            struct rusage usage;
            if (getrusage(RUSAGE_SELF, &usage) == -1)
                throw std::system_error(errno, std::system_category(), "fail to get rusage");
            std::cout << "rusage before time lock: minor=" << usage.ru_minflt << ", major=" << usage.ru_majflt << std::endl;
    #endif // MEMLOCK

            struct sched_param desiredSchedParam;
            int maxAvailPriority;

            prioritySwitched = false;
            currentSchedPolicy = sched_getscheduler(pid);
            if (currentSchedPolicy == -1)
                throw std::system_error(errno, std::system_category(), "fail to get current policy");

            if (sched_getparam(pid, &currentSchedParam) == -1)
                throw std::system_error(errno, std::system_category(), "fail to get current sched priority");

            maxAvailPriority = sched_get_priority_max(desiredSchedPolicy);
            if (maxAvailPriority == -1)
                throw std::system_error(errno, std::system_category(), "fail to get max priority avail");

            desiredSchedParam.__sched_priority = maxAvailPriority - 1;

            if (sched_setscheduler(pid, desiredSchedPolicy, &desiredSchedParam) == -1)
                throw std::system_error(errno, std::system_category(), "fail to set desired sched policy/priority");

            prioritySwitched = true;
        }
        catch(const std::system_error& err)
        {
            std::cout << "Switch to RealTime priority fail: " << err.what() << " : " << strerror(err.code().value()) << std::endl;
        }

    }

    void time_critical_end()
    {
        pid_t pid = 0;
        try {
            if (prioritySwitched)
            {
                if (sched_setscheduler(pid, currentSchedPolicy, &currentSchedParam) == -1)
                    throw std::system_error(errno, std::system_category(), "fail to set original priority");
            }
            prioritySwitched = false;
        }
        catch(const std::system_error& err)
        {
            std::cout << "Switch to RealTime priority fail: " << err.what() << " : " << strerror(err.code().value()) << std::endl;
        }

    #ifdef MEMLOCK
        struct rusage usage;
        if (getrusage(RUSAGE_SELF, &usage) == -1)
            throw std::system_error(errno, std::system_category(), "fail to get rusage");
        std::cout << "rusage after time lock: minor=" << usage.ru_minflt << ", major=" << usage.ru_majflt << std::endl;
    #endif // MEMLOCK
    }
};

bool DHT22Sensor::execute()
{
    bool pollSuccess = false;

    struct timeval pulse_start;
    struct timeval pulse_end;
    struct timeval pulse_delay;
    struct timezone tz;

    uint8_t data[DHT_DATA_BITS / 8];
    memset(data, 0, sizeof(data));

    gpio->setOutput();

    // Data-bus's free status is high voltage level
    gpio->high();
    rpi_delay(DHT_WAKEUP_DELAY_MS);

    TimeCriticalLock lock(SCHED_FIFO);

    // Microprocessor I/O set to output, while output low
    gpio->low();
    // and low hold time can not be less than 800us, typical
    // values are down 1MS
    rpi_delay(DHT_INITIALIZE_DELAY_MS);

    //  then the microprocessor I/O is set to input state
    gpio->setInput();
    //  the release of the bus, due to the
    // pull-up resistor, the microprocessor I/O AM2302 the SDA data line also will be high
    gpio->pudUp();

    size_t bitNum = 0;
    int dhtInitialized = true;

#ifdef DEBUG_DHT
    struct timeval initializationResponseLow;
    struct timeval initializationResponseHigh;
    struct timeval dataTransfer[DHT_DATA_BITS * 2];
    memset(dataTransfer, 0, sizeof(dataTransfer));
#endif // DEBUG_DHT
    struct timeval* delay = &pulse_delay;


    // the AM2302 send a response signal, that is, the output 80 microseconds low as the response signal,
    struct timeval timeout = {0, DHT_TIMEOUT_US};
    try
    {
#ifdef DEBUG_DHT
        delay = &initializationResponseLow;
#endif // DEBUG_DHT
        *delay = gpio->waitPin(GPIO::High, timeout);

    // right then output high of 80 microseconds notice peripheral is ready to receive data signal transmission
#ifdef DEBUG_DHT
        delay = &initializationResponseHigh;
#endif // DEBUG_DHT
        *delay = gpio->waitPin(GPIO::Low, timeout);
    }
    catch(std::overflow_error& err)
    {
        dhtInitialized = false;
    }

    if (dhtInitialized)
    {
        bool validTransfer = true;
        for (bitNum=0; bitNum < DHT_DATA_BITS && validTransfer; bitNum++)
        {
#ifdef DEBUG_DHT
            delay = dataTransfer + (bitNum * 2);
#endif // DEBUG_DHT
            *delay = gpio->waitPin(GPIO::High, timeout);

#ifdef DEBUG_DHT
            delay = dataTransfer + (bitNum * 2 + 1);
#endif // DEBUG_DHT
            *delay = gpio->waitPin(GPIO::Low, timeout);

            data[bitNum/8] <<= 1;
            bool bitValue =  delay->tv_usec > DHT_DATA_BIT1_DELAY_US;
            if (bitValue)
                data[bitNum/8] |= 1;
        }

        if (validTransfer && bitNum == DHT_DATA_BITS)
        {
            if (data[4] != ((data[0] + data[1] + data[2] + data[3]) & 0xFF))
                throw std::runtime_error("checksum fail");
            else
            {
                int t, h;
                h = data[0] * 256 + data[1];
                t = (data[2] & 0x7F) * 256 +  data[3];
                if (data[2] & 0x80)
                    t *= -1;
                setValue(SensorRecord::Temperature, t);
                setValue(SensorRecord::Humidity, h);
                pollSuccess = true;
            }
        }
        else
        {
            std::stringstream os;
            os << "read error (only " << bitNum << " bits read)";
            throw std::runtime_error(os.str());
        }
    }
    else
    {
        throw std::runtime_error("initialization fail");
    }
#ifdef DEBUG_DHT
    std::cout << "response low " << initializationResponseLow << std::endl
              << "response high " << initializationResponseLow << std::endl;
    std::cout << "read bits: " << bitNum << std::endl;
    for(size_t i=0; i< sizeof(dataTransfer) / sizeof(dataTransfer[0]) / 2; i++)
    {
        std::cout << "frame[" << i << "]: " << dataTransfer[i * 2] << " " << dataTransfer[i*2 +1] << std::endl;
    }
#endif // DEBUG_DHT
    return pollSuccess;
}
